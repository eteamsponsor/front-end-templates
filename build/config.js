var defaultOuputPath = '../../web-app/dist/';

module.exports = {
	src: {
		icons: 'img/icons/**/*.svg',
		less: 'less/projects/**/style.less',
		javascript: [
			'js/projects/**/*.js',
			'!js/projects/**/components/*.js'
		],
		templates: 'templates/**/*.jade',
		sync: [
			'data/**/*',
			'img/css/**/*',
			'img/content/**/*'
		]
	},
	dest: {
		default: defaultOuputPath,
		less: defaultOuputPath + 'css/',
		javascript: defaultOuputPath + 'js/',
		templates: defaultOuputPath,
		sync: defaultOuputPath,
		images: defaultOuputPath + 'img/'
	},
	compiled: {
		templates: {
			elasticsearch: [defaultOuputPath + 'elasticsearch/*.html'],
			eteamsponsor: [
				defaultOuputPath + 'campaign-id-login/*.html',
				defaultOuputPath + 'fundraker/*.html'
			],
			esponsornow: [defaultOuputPath + 'payment-flow/*.html']
		},
		images: 'img/**/*'
	},
	watchTasks: [
		{
			name: 'less',
			path: 'less/**/*.less',
			compileOnlyChangedFile: false
		},
		{
			name: 'javascript',
			path: 'js/**/*.js',
			compileOnlyChangedFile: false
		},
		{
			name: 'templates',
			path: 'templates/**/*.jade',
			compileOnlyChangedFile: true
		},
		{
			name: 'sync',
			path: [
				'data/**/*',
				'img/**/*'
			],
			compileOnlyChangedFile: false
		},
		{
			name: 'icons',
			path: ['img/icons/**/*.svg'],
			compileOnlyChangedFile: false
		}
	]
};
