# Build Files

The files contained in this directory drive the Gulp build tasks defined in [gulpfile.js](../gulpfile.js).  There is a `config.js` file and task type files that define functions to be consumed by `gulpfile.js` to execute task types for which the file is named.  The contents of these files and the methodology behind this is detailed below.

## config.js

The `config.js` file contains configuration for all builds and task types such as the locations of their source files, their output destination, and watch tasks.  The export of this file is an object with the following structure:

* `src` - an object defining globs for the source files of a particular task type.  Each task type is a key and its value is either a string or an array of strings.
* `dest` - an object defining the output path for each task type.  Each one is driven off of a `defaultOutputPath` variable defined at the top of the file so you can easily change where all files are output.
* `compiled` - an object with globs for compiled files of a certain type.  These are utilized by dist tasks to execute additional tasks on the compiled files, such as compress images or search HTML for unused CSS.
    * `templates` - an object whose keys are project names.  These project names are used to organize project-specific Less, JavaScript, and template files and are used here to define where their associated templates are compiled.  Note that some projects, such as `eteamsponsor`, have more than one set of templates.  In the case of `eteamsponsor` it has both Campaign ID Login and FundRaker templates because these templates use the same CSS and JavaScript and are thus part of the same "project."
* `watchTasks` - an array of objects containing the `name`, glob `path`, and `compileOnlyChangedFile` settings for the task type's associated watch task.

If you ever create a new task type or project, make sure to update this file to keep configuration clean and separate from the actual task implementations.

## [task-type].js

The files for the different task types, such as `images` or `javascript`, have common functions that are exported from the file so they can be executed in [gulpfile.js](../gulpfile.js).

* `getStream` (required) - creates a Gulp stream based on the configuration source.  This function may also perform some additional manipulation to the stream that is common to both the `defaultTask` and `distTask` as defined below.
* `writeStream` (required) - outputs the Gulp stream to the configured destination directory.
* `defaultTask` (optional) - gets a stream from `getStream` and writes it to the configured destination directory with `writeStream`.  This is the task that is run with the default and watch Gulp builds.  This is optional because some task types, such as `images`, don't requite a default task.
* `distTask` (optional) - gets a stream from `getStream`, performs additional manipulations to the stream,  and writes it to the configured destination directory with `writeStream`.  This is the task that is run with the dist Gulp build.  This is optional because some task types, such as `sync`, don't requite a default task.

## Task Types

These are the task types that have their own files:

* `icons` - creates a single SVG icon sprite.
* `images` - minifies images used for CSS, content, and favicons.
* `javascript` - compiles JavaScript files with Browserify.  The `distTask` also minifies the output.
* `less` - compiles Less files into CSS that's readable by the browser.  The `distTask` also removes unused CSS styles, cleans it, and hashes references to static assets, such as images, for cache-busting.
* `sync` - keeps static assets such as images and sample data in sync between the source and destination directories.
* `templates` - compiles Jade files into HTML.
