var gulp = require('gulp'),
	g = require('gulp-load-plugins')(),
	config = require('./config');

module.exports = {
	getStream: function() {
		return gulp.src(config.compiled.images);
	},

	writeStream: function(stream) {
		return stream.pipe(gulp.dest(config.dest.images));
	},

	distTask: function() {
		var stream = module.exports.getStream();

		stream
			.pipe(g.bytediff.start())
			.pipe(g.imagemin())
			.pipe(g.bytediff.stop());

		return module.exports.writeStream(stream);
	}
};