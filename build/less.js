var gulp = require('gulp'),
	g = require('gulp-load-plugins')(),
	mergeStream = require('merge-stream'),
	config = require('./config');

module.exports = {
	getStream: function(path) {
		if (typeof path === 'undefined') {
			path = config.src.less;
		}

		return gulp.src(path)
			.pipe(g.less());
	},

	writeStream: function(stream, path) {
		if (typeof path === 'undefined') {
			path = config.dest.less;
		}

		return stream.pipe(gulp.dest(path));
	},

	defaultTask: function() {
		var stream = module.exports.getStream();

		return module.exports.writeStream(stream);
	},

	// http://stackoverflow.com/a/34708546/1489958
	distTask: function() {
		var projects = [],
			destPath,
			srcPath,
			templatesPath;

		for (var project in config.compiled.templates) {
			destPath = config.dest.less + project;
			srcPath = destPath + '/style.css';
			templatesPath = config.compiled.templates[project];

			projects.push(
				gulp.src(srcPath)
					.pipe(g.uncss({
						html: templatesPath,
						ignore: [
							// Added by Bootstrap validator
							/disabled/,
							/.has\-error/,
							// Added by JavaScript
							/js/,
							/open/,
							/show/,
							/hidden/,
							/is-non-empty/,
							/is-focused/,
							/is-expanded/,
							/is-visible/,
							/is-max-width/,
							/collapsing/,
							/file-reader-support/,
							/twitter-typeahead/,
							/typeahead/,
							/tt-.*/,
							/modal/,
							/modal-*/,
							/visuallyhidden/,
							/no-touchevents/,
							/no-ios/,
							/ios/,
							// for dynamic Handlebars templates used on Update and Add User templates
							/multiple/,
							// Not recognized otherwise
							/-webkit-inner-spin-button/,
							/-webkit-outer-spin-button/,
							/oldie/,
							/no-js/,
							/landing/,
							// for Bootstrap carousel
							/next/,
							/prev/,
							/active/,
							/right/,
							/left/
						]
					}))
					.pipe(g.cleanCss())
					.pipe(g.cssHashes({
						assetsPath: config.dest.less + "project-name",	// since CSS files are put into project specific folders, we need to append a string (e.g. "project-name") to represent that project's directory
						allowMissingFiles: true
					}))
					.pipe(gulp.dest(destPath))
			);
		}

		return mergeStream(projects);
	}
};
