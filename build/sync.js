// http://stackoverflow.com/a/29880631
var gulp = require('gulp'),
	g = require('gulp-load-plugins')(),
	config = require('./config');

module.exports = {
	getStream: function(path) {
		if (typeof path === 'undefined') {
			path = config.src.sync;
		}

		return gulp.src(path, { "base": "./" })
			.pipe(g.newer(config.dest.sync));
	},

	writeStream: function(stream) {
		return stream.pipe(gulp.dest(config.dest.sync));
	},

	defaultTask: function() {
		var stream = module.exports.getStream();

		return module.exports.writeStream(stream);
	}
};