var gulp = require('gulp'),
	g = require('gulp-load-plugins')(),
	config = require('./config');

module.exports = {
	getStream: function() {
		var svgSpriteConfig = {
			mode: {
				symbol: {
					dest: '',
					sprite: 'icons.svg'
				}
			},
			shape: {
				id: {
					generator: '%s-icon'
				}
			}
		};

		return gulp.src(config.src.icons)
			.pipe(g.svgSprite(svgSpriteConfig));
	},

	writeStream: function(stream) {
		return stream.pipe(gulp.dest(config.dest.images));
	},

	defaultTask: function() {
		var stream = module.exports.getStream();

		return module.exports.writeStream(stream);
	}
};
