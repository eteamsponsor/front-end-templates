var gulp = require('gulp'),
	g = require('gulp-load-plugins')(),
	config = require('./config'),
	baseDirectory = './templates';

module.exports = {
	getStream: function(path) {
		if (typeof path === 'undefined') {
			path = config.src.templates;

			return gulp.src(path, { base: baseDirectory })
				.pipe(g.jade());
		}
		else {
			return gulp.src(path, { base: baseDirectory })
				.pipe(g.jadeInheritance({ basedir: baseDirectory }))
				.pipe(g.jade());
		}
	},

	writeStream: function(stream) {
		return stream.pipe(gulp.dest(config.dest.templates));
	},

	defaultTask: function() {
		var stream = module.exports.getStream();

		return module.exports.writeStream(stream);
	}
};
