// https://github.com/gulpjs/gulp/blob/master/docs/recipes/browserify-multiple-destination.md
var gulp = require('gulp'),
	g = require('gulp-load-plugins')(),
	browserify = require('browserify'),
	config = require('./config');

module.exports = {
	getStream: function() {
		return gulp.src(config.src.javascript, { read: false })
			.pipe(g.tap(function(file) {
				file.contents = browserify(file.path, { debug: false }).bundle();
			}))
			.pipe(g.buffer());
	},

	writeStream: function(stream) {
		return stream.pipe(gulp.dest(config.dest.javascript));
	},

	defaultTask: function() {
		var stream = module.exports.getStream();

		return module.exports.writeStream(stream);
	},

	distTask: function() {
		var stream = module.exports.getStream();

		stream
			.pipe(g.uglify());

		return module.exports.writeStream(stream);
	}
};