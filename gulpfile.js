var gulp = require('gulp'),
    g = require('gulp-load-plugins')(),
    awsConfig,
    s3,
    runSequence = require('run-sequence'),
    del = require('del'),
    config = require('./build/config'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,
    tasks = {
        'less': require('./build/less'),
        'javascript': require('./build/javascript'),
        'sync': require('./build/sync'),
        'templates': require('./build/templates'),
        'images': require('./build/images'),
        'icons': require('./build/icons')
    };

//
// DEV/WATCH TASKS
//
gulp.task('default', ['icons', 'less', 'javascript', 'sync', 'templates']);

gulp.task('icons', function () {
    return tasks.icons.defaultTask();
});

gulp.task('less', function () {
    return tasks.less.defaultTask();
});

gulp.task('javascript', function () {
    return tasks.javascript.defaultTask();
});

gulp.task('sync', function () {
    return tasks.sync.defaultTask();
});

gulp.task('templates', function () {
    return tasks.templates.defaultTask();
});

gulp.task('s3-upload', function () {
    awsConfig = require('./private/awsaccess');
    s3 = require('gulp-s3-upload')(awsConfig.s3Upload);

    return gulp.src(config.dest.default + '/**/*')
        .pipe(s3({
            Bucket: 'ui.eteamsponsor.com',
            ACL: 'public-read'
        }));
});

gulp.task('watch', ['icons', 'less', 'javascript', 'sync', 'templates'], function () {
    browserSync.init({
        server: {
            baseDir: config.dest.default
        },
        // that's will disable cross tab sync
        ghostMode: false,
        notify: false
    });

    config.watchTasks.forEach(function (task) {
        gulp.watch(task.path, function (event) {
            var taskType = tasks[task.name];

            var stream = (task.compileOnlyChangedFile) ? taskType.getStream(event.path) : taskType.getStream();

            taskType
                .writeStream(stream)
                .pipe(reload({stream: true}));
        });
    });
});

//
// DIST TASKS
//

gulp.task('upload', function () {
    runSequence(
        'clean',
        ['icons', 'less', 'javascript-dist', 'sync', 'templates'],
        'less-dist',
        'images-dist',
        's3-upload'
    );
});

gulp.task('dist', function () {
    runSequence(
        'clean',
        ['icons', 'less', 'javascript-dist', 'sync', 'templates'],
        'less-dist',
        'images-dist'
    );
});

gulp.task('clean', function () {
    return del(config.dest.default, {force: true});
});

gulp.task('less-dist', function () {
    return tasks.less.distTask();
});

gulp.task('javascript-dist', function () {
    return tasks.javascript.distTask();
});

gulp.task('images-dist', function () {
    return tasks.images.distTask();
});
