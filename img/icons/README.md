# Icons

To add a new icon to the project, simply place its SVG file in this directory and name it accordingly.  The name you give it should be all lowercase with hyphens separating words for readability and consistency.  This is the name you will use in the markup when referencing the xlink.  For more information about the process, you can read the [blog post that inspired this work](https://cloudfour.com/thinks/our-svg-icon-process/) and the [related Asana task](https://app.asana.com/0/80660851875913/433598625376629).
