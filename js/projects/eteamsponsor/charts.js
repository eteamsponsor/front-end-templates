/* globals chartConfig */
var $ = require('jquery');
var Chart = require('chart.js');
var debounce = require('../../components/debounce');
var browser = require('../../components/browser');
var textTransform = require('../../components/textTransform');

Chart.defaults.global.elements.line.fill = false;

$(function() {
	var charts = {},
		sizeMap = {
			'extraSmall': 'small',
			'small': 'medium',
			'medium': 'large',
			'large': 'large'
		};	// maps a Bootstrap standard breakpoint to dataset size and aspect ratio

	function renderCharts(browserSize) {
		$('[data-chart-name]').each(function() {
			var chartCtx = $(this),
				chartName = chartCtx.data('chart-name'),
				dynamicDataset = chartCtx.data('chart-dynamic-dataset') === true,
				beginAtZero = chartCtx.data('chart-begin-at-zero') === true,
				datasetSize = sizeMap[browserSize],
				dataset = dynamicDataset ? chartConfig[chartName]['data'][datasetSize] : chartConfig[chartName].data,
				baseChartSize = 100,
				aspectRatios = chartConfig[chartName]['aspectRatios'],
				aspectRatio = (typeof aspectRatios !== 'undefined') ? aspectRatios[sizeMap[browserSize]] : null;

			if (aspectRatio) {
				chartCtx[0].width = aspectRatio.split(':')[0] * baseChartSize;
				chartCtx[0].height = aspectRatio.split(':')[1] * baseChartSize;
			}

			// destroy chart instances before re-rendering them to avoid the tooltip hover issue
			if (typeof charts[chartName] !== 'undefined') {
				charts[chartName].destroy();
			}

			charts[chartName] = new Chart(chartCtx, {
				type: chartCtx.data('chart-type'),
				data: dataset,
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: beginAtZero,
								callback: function(value, index, values) {
									return '$' + textTransform.toCurrency(value);
								}
							}
						}]
					},
					tooltips: {
						mode: 'label',
						callbacks: {
							label: function(tooltipItem, data) {
								return '$' + textTransform.toCurrency(tooltipItem.yLabel);
							}
						},
						titleFontSize: 14,
						bodyFontSize: 14
					}
				}
			});
		});
	}

	var reRenderCharts = debounce(function() {
		renderCharts(browser.getSize());
	}, 250);

	window.addEventListener('resize', reRenderCharts);

	renderCharts(browser.getSize());
});