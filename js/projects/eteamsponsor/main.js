/* global require */
global.$ = require('jquery');
global.jQuery = global.$;
require('../../components/ie10-viewport-bug-workaround');
require('../../components/svgxuse');
require('../../components/transition');
require('../../components/modal');
require('../../components/alert');
require('../../components/dropdown');
require('../../components/tab');
require('../../components/collapse');
require('../../components/sharing')({
    facebookAppId: '108206192561870'
});
require('./components/forms');
require('./components/interruptions');
require('./components/navigation');
require('./components/overlay');
require('./components/clipboard');
require('./components/tiles');
require('./components/campaign-typeform');
require('./components/campaign-typeform-edc');
require('./components/campaign-landing');
require('./components/campaign-emails');
require('./components/campaign-emails-edc');
require('./components/campaign-edit');
require('./components/campaign-vimeo');
require('./components/carousel');
require('../elasticsearch/main');
