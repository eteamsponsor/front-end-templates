var $ = require('jquery');

$('.tile-progress').each(function() {
	var $progressBar = $(this);
	var progressValue = $progressBar.data('value').slice(0, -1);

	if (progressValue >= 100) {
		$progressBar.addClass('is-max-width');
	}
});