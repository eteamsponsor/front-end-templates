var $ = require('jquery');
var browser = require('../../../components/browser');
var breakpoints = require('../../../components/breakpoints');
var debounce = require('../../../components/debounce');
var previousBrowserSize = browser.getSize();

var navigation = {
	init: function() {
		// cache jQuery objects
		navigation.navPrimary = $('.nav-primary');
		navigation.navMegaMenu = $('.nav-mega-menu');
		navigation.navMegaMenuOpenTriggers = $('.nav-mega-menu-open-trigger');
		navigation.navMegaMenuCloseTriggers = $('.nav-mega-menu-close-trigger');
		navigation.dropdownToggleTriggers = $('.nav-utility [data-toggle="dropdown"], .layout-primary [data-toggle="dropdown"]');

		navigation.bindEvents();
	},

	bindEvents: function() {
		navigation.navMegaMenuOpenTriggers.on('click', function(e) {
			e.preventDefault();

			var $trigger = $(this);
			var $megaMenu = $trigger.siblings('.nav-mega-menu');

			// the viewport must be frozen because the navigation takes over the entire screen at low resolutions
			if (browser.getSize() === 'extraSmall') {
				browser.freezeViewport();
			}

			navigation.dropdownToggleTriggers.dropdown('hide');
			navigation.toggleMegaMenu($trigger, $megaMenu);
		});

		navigation.navMegaMenuCloseTriggers.on('click', function(e) {
			e.preventDefault();

			var $trigger = $(this);
			var $megaMenu = $trigger.parent('.nav-mega-menu');

			// the viewport must be thawed because the navigation takes over the entire screen at low resolutions
			if (browser.getSize() === 'extraSmall') {
				browser.thawViewport();
			}

			navigation.dropdownToggleTriggers.dropdown('hide');
			navigation.toggleMegaMenu($trigger, $megaMenu);
		});

		navigation.dropdownToggleTriggers.on('click.bs.dropdown', function() {
			navigation.closeAllMenus();
		});

		// this will handle clicks "outside" of the mega menus and close the menus
		$(document).on('click', function(e) {
			if (!$(e.target).closest('.nav-primary').length) {
				navigation.closeAllMenus();
			}
		});

		window.addEventListener('resize', navigation.checkState);
	},

	checkState: debounce(function() {
		var currentBrowserSize = browser.getSize();
		var sizeChanged = currentBrowserSize !== previousBrowserSize;

		previousBrowserSize = currentBrowserSize;

		if (sizeChanged) {
			navigation.closeAllMenus();
			browser.thawViewport();
		}
	}, 100),

	closeAllMenus: function($trigger, $megaMenu) {
		navigation.navMegaMenuOpenTriggers
			.not($trigger)
			.removeClass('is-expanded');

		navigation.navMegaMenu
			.not($megaMenu)
			.removeClass('is-visible');
	},

	toggleMegaMenu: function($trigger, $megaMenu) {
		$trigger.toggleClass('is-expanded');

		$megaMenu.toggleClass('is-visible');

		navigation.closeAllMenus($trigger, $megaMenu);
	}
};

navigation.init();

module.exports = {
	closeAllMenus: navigation.closeAllMenus
};
