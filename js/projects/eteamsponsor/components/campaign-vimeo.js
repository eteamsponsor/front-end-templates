var $ = require('jquery');
var vimeo = require('../../../components/vimeo-upload');
require('../../campaign-page/components/video');

$(document).ready(function () {

	var dashboard = $('#program-dashboard-campaigns');
	var emails = $('#campaign-emails-template');
	// Called when files are selected by the browse button.
	// For each file, uploads the content to Drive & displays the results when complete.
	if (top.location.pathname === '/fundraker/campaign-emails.html' || top.location.pathname === '/fundraker/program-dashboard.html') {
		if (dashboard.length > 0 || emails.length > 0) {

			function handleFileSelect(evt) {
				evt.stopPropagation();
				evt.preventDefault();
				var files = evt.dataTransfer ? evt.dataTransfer.files : $(this).get(0).files;
				var results = document.getElementById('resultsVimeo');
				// Clear the results div
				while (results.hasChildNodes()) results.removeChild(results.firstChild);
				// Rest the progress bar and show it
				updateProgress(0);
				document.getElementById('progress-container').style.display = 'block';
				$('#uploadVideoDropzone').hide();
				if (checkAcceptedType(files[0].type)) {
					// Instantiate Vimeo Uploader
					(new vimeo({
						name: document.getElementById('videoName').value,
						description: document.getElementById('videoDescription').value,
						privacy: 'anybody',
						file: files[0],
						token: '5b82402c2c77ee87f6d8ea7f302e3d26',
						onError: function (data) {
							showMessage('<strong>Error</strong>: ' + JSON.parse(data).error, 'danger');
							$('#uploadVideoDropzone').show();
						},
						onProgress: function (data) {
							updateProgress(data.loaded / data.total)
						},
						onComplete: function (videoId, index) {
							if (index > -1) {
								// The metadata contains all of the uploaded video(s) details see: https://developer.vimeo.com/api/endpoints/videos#/{video_id}
								urlFull = this.metadata[index].link;
								// add stringify the json object for displaying in a text area
								$('#vimeoVideo').val(urlFull);
							}
							$('#uploadVideoDropzone').show();
							showMessage('<strong>Upload Successful</strong>: check uploaded video @ <a href="' + urlFull + '">' + urlFull + '</a>.')
						}
					})).upload();
				} else {
					showMessage('<strong>Error</strong>: Wrong format', 'danger')
				}

				// Check input format
				function checkAcceptedType(type) {
					var accepted = ['mov', 'wmv', 'avi', 'flv', 'mp4'];
					var videoType = type.split('/').pop();
					for (var i = 0; i < accepted.length; i++) {
						if (accepted[i] === videoType) {
							return true
						}
					}
					return false
				}


				// local function: show a user message
				function showMessage(html, type) {
					// hide progress bar
					document.getElementById('progress-container').style.display = 'none';
					// display alert message
					var element = document.createElement('div');
					element.setAttribute('class', 'alert alert-' + (type || 'success'));
					element.innerHTML = html;
					results.appendChild(element);
					// Remove message after 5 seconds
					setTimeout(function () {
						results.innerHTML = "";
						$('#uploadVideoDropzone').show();
					}, 5000)
				}
			}

			function getCampaignFromStorage() {
				var campaignObject = localStorage.getItem('campaign');
				return JSON.parse(campaignObject);
			}

			// Update progress bar.
			function updateProgress(progress) {
				progress = Math.floor(progress * 100);
				var element = document.getElementById('progress');
				element.setAttribute('style', 'width:' + progress + '%');
				element.innerHTML = '&nbsp;' + progress + '%'
			}

			// Get video id
			function getVideoId(string) {
				return string.split('/').pop();
			}

			// Set campaign video
			function setCampaignVideo() {
				var campaign = getCampaignFromStorage();
				var campaignVideo = $('#campaign-video');
				if (campaign.teamVideo) {
					var id = getVideoId(campaign.teamVideo);
					campaignVideo.attr('src', '//player.vimeo.com/video/' + id);
				} else {
					campaignVideo.hide();
				}
			}

			$('.saveLandingTemplateBtn').click(function () {
				setCampaignVideo();
			});
			// Wire up browse input file change
			$('#browse').on('change', handleFileSelect);

			// Re click for a drop base
			$('#uploadVideoDropzone').click(function () {
				$('#browse').click();
			});

			// Preload video NAME and DESCRIPTION
			(function () {
				$('#videoName').val('Challenge Video');
				$('#videoDescription').val('Video Description');
				$('#progress-container').hide();
				setCampaignVideo();
			})()
		}
	}
});
