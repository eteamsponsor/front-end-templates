var $ = require('jquery');
var bootbox = require('bootbox');
var Quill = require('quill');

$(document).ready(function(){

	var campaignEmailsAnchor = $('#campaign-emails-template');

	if (top.location.pathname === '/fundraker/campaign-emails-edc.html') {

		if (campaignEmailsAnchor.length > 0) {

			function EmailConfig() {
				// Global config editor
				var editor;

				this.initState = {
					suppressFirstDonorDate: false,
					suppressSecondDonorDate: false,
					suppressThirdDonorDate: false,
					suppressFourDonorDate: false,
					suppressFiveDonorDate: false,
					suppressPersonal: false,
					suppressScheduled: false,
					welcomeEmail: {
						showIntroductory: true,
						showPayment: true,
						showClosing: true,
						overwriteTemplate: false,
						overwriteMessage: {
							"ops": [
								{
									"insert": "I am excited to be participating in soccer club this fall for SHU Athletics. \n " +
										"My personal fundraising goal is to raise $100 for our program."

								}
							]
						},
						adminMessage: {
							"ops": [
								{
									"insert": "I am excited to be participating in soccer club this fall for SHU Athletics. \n " +
										"My personal fundraising goal is to raise $100 for our program."

								}
							]
						}
					},
					reminderEmail: {
						showIntroductory: true,
						showPayment: true,
						showClosing: true,
						overwriteTemplate: false,
						overwriteMessage: {
							"ops": [
								{
									"insert": "I am excited to be participating in soccer club this fall for SHU Athletics. \n " +
										"My personal fundraising goal is to raise $100 for our program."

								}
							]
						},
						adminMessage: {
							"ops": [
								{
									"insert": "I am excited to be participating in soccer club this fall for SHU Athletics. \n " +
										"My personal fundraising goal is to raise $100 for our program."

								}
							]
						}
					},
					resultsEmail: {
						showIntroductory: true,
						showPayment: true,
						showClosing: true,
						overwriteTemplate: false,
						overwriteMessage: {
							"ops": [
								{
									"insert": "I am excited to be participating in soccer club this fall for SHU Athletics. \n " +
										"My personal fundraising goal is to raise $100 for our program."
								}
							]
						},
						adminMessage: {
							"ops": [
								{
									"insert": "I am excited to be participating in soccer club this fall for SHU Athletics. \n " +
										"My personal fundraising goal is to raise $100 for our program."

								}
							]
						}
					},
					immediateEmail: {
						showIntroductory: true,
						showPayment: true,
						showClosing: true,
						overwriteTemplate: false,
						overwriteMessage: {
							"ops": [
								{
									"insert": "Thank you for your donation to 2018 SHU Women's Soccer Challenge . Your donation is greatly appreciated and it has helped us get one step closer to our $10,000 goal!Be sure to follow our campaign's success and share it with others by visiting "
								},
								{
									"attributes": {
										"color": "#0e137e",
										"background": "#ffffff",
										"link": "http://testapp.eteamsponsor.com/ETS/supportUs/2841884?fund_participant_id=2841886"
									},
									"insert": "our campaign page."
								}
							]
						},
						adminMessage: {
							"ops": [
								{
									"insert": "I am excited to be participating in soccer club this fall for SHU Athletics. \n " +
										"My personal fundraising goal is to raise $100 for our program."

								}
							]
						}
					},
					personalEmail: {
						showIntroductory: true,
						showPayment: true,
						showClosing: true,
						overwriteTemplate: false,
						overwriteMessage: {
							"ops": [
								{
									"insert": "Thank you for your donation to 2018 SHU Women's Soccer Challenge. Your donation is greatly appreciated and it has helped us get one step closer to our $10,000 goal! Be sure to follow our campaign's success and share it with others by visiting "
								},
								{
									"attributes": {
										"color": "#0e137e",
										"background": "#ffffff",
										"link": "http://testapp.eteamsponsor.com/ETS/supportUs/2841884?fund_participant_id=2841886"
									},
									"insert": "our campaign page."
								}
							]
						},
						adminMessage: {
							"ops": [
								{
									"insert": "I am excited to be participating in soccer club this fall for SHU Athletics. \n " +
										"My personal fundraising goal is to raise $100 for our program."

								}
							]
						}
					},
					scheduledEmail: {
						showIntroductory: true,
						showPayment: true,
						showClosing: true,
						overwriteTemplate: false,
						overwriteMessage: {
							"ops": [
								{
									"insert": "Some test text"
								},
								{
									"attributes": {
										"color": "#0e137e",
										"background": "#ffffff",
										"link": "/ETS/supportUs/2841884?fund_participant_id=2841886"
									},
									"insert": "test"
								}
							]
						},
						adminMessage: {
							"ops": [
								{
									"insert": "I am excited to be participating in soccer club this fall for SHU Athletics. \n " +
										"My personal fundraising goal is to raise $100 for our program."

								}
							]
						}
					},
					defaultMessage: {
						"ops": [
							{
								"insert": "I am excited to be participating in soccer club this fall for SHU Athletics. \n " +
									"My personal fundraising goal is to raise $100 for our program."

							}
						]
					}
				};
				this.loadInitToStorage = function () {
					var storage = localStorage.getItem('emails');
					if (!storage) {
						// Hide campaign row
						$('#newCampaignRow').hide();
						// Set to storage init values
						localStorage.setItem('emails', JSON.stringify(this.initState));
					} else if (storage === undefined) {
						// In case of localStorage bad init
						localStorage.clear();
					} else {
						console.log('Local Storage for a Emails: We good.')
					}
				};
				this.getCampaignFromStorage = function () {
					var campaignObject = localStorage.getItem('campaign');
					return JSON.parse(campaignObject);
				};
				this.getEmailSettingsFromStorage = function () {
					var emailsObject = localStorage.getItem('emails');
					return JSON.parse(emailsObject);
				};
				this.saveEmailSettingsFromStorage = function (obj) {
					localStorage.setItem('emails', JSON.stringify(obj));
				};
				this.saveCampaignFromStorage = function (obj) {
					localStorage.setItem('campaign', JSON.stringify(obj));
				};
				this.toggleView = function (selectors) {
					for (var i = 0; i < arguments.length; i++) {
						$(arguments[i]).toggle();
					}
				};
				this.toggleBorderedClass = function (selectors) {
					for (var i = 0; i < arguments.length; i++) {
						$(arguments[i]).toggleClass('bordered-editor');
					}
				};
				this.setMinToDateInput = function (selector) {
					var today = new Date().toISOString().split('T')[0];
					$(selector).attr('min', today);
				};
				this.updateShowButtons = function (selector, configValue, emailType) {
					var EMAIL_STATE = this.getEmailSettingsFromStorage();
					var typed = EMAIL_STATE[emailType];
					if (typed[configValue] === true) {
						$(selector).hide();
					}
				};
				this.togglePreviewSection = function (selector, section) {
					$(selector).change(function () {
						$(section).toggle(this.checked);
					}).change();
				};
				this.editEmailText = function (selector, text) {
					$(selector).text(text);
				};
				this.fillSuppressCheckboxes = function (selector, value) {
					$(selector).attr('checked', value);
				};
				this.suppressEmailChange = function (selector, obj) {
					$('#' + selector).change(function () {
						if ($(this).is(':checked')) {
							suppressEmail(selector, true, obj);
						} else {
							suppressEmail(selector, false, obj);
						}
					})
				};
				this.openEmail = function (openSelector, closeSelector, target) {
					$(closeSelector).hide();
					if (openSelector === 'direct') {
						$(target).collapse('show');
					} else {
						$(openSelector).click(function () {
							$(target).collapse('show');
						});
						$(openSelector + 'Link').click(function () {
							$(target).collapse('show');
						});
						$(closeSelector).click(function () {
							$(target).collapse('hide');
						});
					}
					$(target).on('hide.bs.collapse', function () {
						$(openSelector).show();
						$(closeSelector).hide();
					});
					$(target).on('show.bs.collapse', function () {
						$(openSelector).hide();
						$(closeSelector).show();
					});
				};
				this.reclickAndHide = function (selector, target) {
					$(selector).click(function () {
						$(target).click();
						$(this).hide();
					})
				};
				this.saveUpdatedKeyDates = function () {
					var campaign = this.getCampaignFromStorage();
					var date = $('#edit-date').val();
					var kd1 = getKeyDate(date, 7);
					var kd2 = getKeyDate(date, 21);
					var kd3 = getKeyDate(date, 35);
					var kd4 = getKeyDate(date, 45);
					campaign.campaignKeyDates.emailCampaignBeginsDate = date;
					campaign.campaignKeyDates.emailTouch2Date = kd1;
					campaign.campaignKeyDates.emailTouch3Date = kd2;
					campaign.campaignKeyDates.emailTouch4Date = kd3;
					campaign.campaignKeyDates.emailTouch5Date = kd4;
					this.saveCampaignFromStorage(campaign);
				};
				this.fillBodyText = function (selector, type, editionType) {
					var emails = this.getEmailSettingsFromStorage();
					var typed = emails[type];
					var body;
					if (editionType) {
						var edition = typed[editionType];
						editor.setContents(edition);
						body = editor.root.innerHTML;
					} else {
						if (editor) {
							editor.setContents(typed.overwriteMessage);
							body = editor.root.innerHTML;
						} else {
							body = typed.overwriteMessage.ops[0].insert;
						}
					}
					var defaultMessage = emails.defaultMessage.ops[0].insert;
					if (isEmpty(body)) {
						$(selector).html(defaultMessage);
					} else {
						$(selector).html(body);
					}
				};
				this.fillStartDate = function (selector) {
					var campaign = this.getCampaignFromStorage();
					var date = campaign.campaignKeyDates.emailCampaignBeginsDate;
					$(selector).val(date);
					updateDates(date);
				};
				this.saveEditedTemplate = function (introductorySelector,
													paymentSelector,
													closingSelector,
													overwriteSelector,
													emailType) {
					var config = this.getEmailSettingsFromStorage();
					var typed = config[emailType];

					// Check if show / hide checkbox checked
					var introductory = $(introductorySelector).is(':checked');
					var payment = $(paymentSelector).is(':checked');
					var closing = $(closingSelector).is(':checked');
					var overwrite = $(overwriteSelector).is(':checked');

					// Rewrite config object
					typed.showIntroductory = introductory;
					typed.showPayment = payment;
					typed.showClosing = closing;
					typed.overwriteTemplate = overwrite;

					// Save updated values
					this.saveEmailSettingsFromStorage(config);
				};
				// Hide section and show span row
				this.toggleSection = function (hideButton,
											   checkId,
											   hideSection,
											   showRow) {
					$(hideButton).click(function () {
						$(checkId).attr('checked', false);
						$(hideSection).hide();
						$(showRow).show();
					})
				};
				this.toggleEditView = function (buttonId,
												hideSectionId,
												showSectionId,
												changeAttrId,
												changeValue) {
					$(buttonId).click(function () {
						$(hideSectionId).hide();
						$(showSectionId).show();
						$(changeAttrId).attr('checked', changeValue);
					})
				};
				this.updateEditedText = function (buttonId, selector, type) {
					var self = this;
					$(buttonId).click(function () {
						self.fillBodyText(selector, type);
					})
				};
				this.initRTE = function (selector, type) {
					var rte = document.getElementById(selector);
					if (rte) {
						editor = new Quill(rte, {
							modules: {
								toolbar: true
							},
							theme: 'snow'
						});
					}
					// temporary fix
					if (type === 'scheduledEmail') {
						editor.setText('Some test text');
					}
				};
				this.loadRTEContent = function (type, editionType) {
					setTimeout(function () {
						var local = JSON.parse(localStorage.getItem('emails'));
						var typed = local[type];
						if (typed) {
							var message = typed[editionType];
							if (isEmpty(message.ops[0].insert)) {
								editor.setContents(local.defaultMessage);
							} else {
								editor.setContents(message);
							}
						}
					}, 100);
				};
				this.saveRTEContentListener = function (button, type, target, close, editionType) {
					var self = this;
					var emails = this.getEmailSettingsFromStorage();
					var typed = emails[type];
					$(button).click(function () {
						typed[editionType] = editor.getContents();
						localStorage.setItem('emails', JSON.stringify(emails));
						self.fillBodyText(target, type, editionType);
						$(close).click();
					});
				};
			}

			// FUNCTIONS AND HELPERS
			function toggleWelcomeEdit(obj) {
				obj.toggleView(
					'#saveWelcomeTemplateBtn',
					'#cancelWelcomeTemplateBtn',
					'#emailEditDate',
					'.hidden-welcome-edit',
					'#rtc-editor'
				);
				obj.toggleBorderedClass(
					'.welcome-letter-body',
					'.preview-welcome-body-info',
					'.preview-welcome-edit-template',
					'.welcome-payment-methods',
					'.welcome-closing-paragraph'
				);
			}

			function toggleEmailEdit(obj, selector) {
				var big = capitalizeFirstLetter(selector);
				obj.toggleView(
					'#save' + big + 'TemplateBtn',
					'#cancel' + big + 'TemplateBtn',
					'.hidden-' + selector + '-edit',
					'.reminder-' + selector + '-show',
					'#' + selector + '-rtc-editor'
				);
				obj.toggleBorderedClass(
					'.' + selector + '-body',
					'.preview-' + selector + '-body-info',
					'.preview-' + selector + '-edit-template',
					'.' + selector + '-payment-methods',
					'.' + selector + '-closing-paragraph'
				);
			}

			function updateControls(obj, emailType) {
				var templateStatus = obj.getEmailSettingsFromStorage();
				var typed = templateStatus[emailType];
				$('#show-introductory').prop('checked', typed.showIntroductory).change();
				$('#show-payment').prop('checked', typed.showPayment).change();
				$('#show-closing').prop('checked', typed.showClosing).change();
				$('#show-override-template').prop('checked', typed.overwriteTemplate);
			}

			function updateEmailControls(obj, emailType, selector) {
				var templateStatus = obj.getEmailSettingsFromStorage();
				var typed = templateStatus[emailType];
				$('#' + selector + '-show-introductory').prop('checked', typed.showIntroductory).change();
				$('#' + selector + '-show-payment').prop('checked', typed.showPayment).change();
				$('#' + selector + '-show-closing').prop('checked', typed.showClosing).change();
				$('#' + selector + '-show-override-template').prop('checked', typed.overwriteTemplate);
			}

			function updateKeyDates(obj) {
				var campaign = obj.getCampaignFromStorage();
				var email = obj.getEmailSettingsFromStorage();
				if (campaign !== null) {
					var importantDates = campaign.campaignKeyDates;
				}
				if (importantDates) {
					if (campaign !== null) {
						obj.editEmailText('.emailFirstDate', importantDates.emailCampaignBeginsDate);
						obj.editEmailText('.emailSecondDate', importantDates.emailTouch2Date);
						obj.editEmailText('.emailThirdDate', importantDates.emailTouch3Date);
						obj.editEmailText('.emailFourDate', importantDates.emailTouch4Date);
						obj.editEmailText('.emailFiveDate', importantDates.emailTouch5Date);
					}
					if (email !== null) {
						obj.fillSuppressCheckboxes('#suppressFirstDonorDate', email.suppressFirstDonorDate);
						obj.fillSuppressCheckboxes('#suppressSecondDonorDate', email.suppressSecondDonorDate);
						obj.fillSuppressCheckboxes('#suppressThirdDonorDate', email.suppressThirdDonorDate);
						obj.fillSuppressCheckboxes('#suppressFourDonorDate', email.suppressFourDonorDate);
						obj.fillSuppressCheckboxes('#suppressFiveDonorDate', email.suppressFiveDonorDate);
						obj.fillSuppressCheckboxes('#suppressPersonal', email.suppressPersonal);
						obj.fillSuppressCheckboxes('#suppressScheduled', email.suppressScheduled);
					}
				}
			}
			function fillCampaignName(obj){
				var campaign = obj.getCampaignFromStorage();
				var campaignSelector = $('.preview-email-campaign-name');
				campaignSelector.text(campaign.campaignName);
			}
			function suppressEmail(selector, status, obj) {
				var message;
				if (status === true) {
					message = "The email won’t be sent. Are you sure?";
				} else {
					message = "The email will be sent. Are you sure?";
				}
				bootbox.confirm(message, function (result) {
					var emails = obj.getEmailSettingsFromStorage();
					if (result === true) {
						emails[selector] = $('#' + selector).is(':checked');
						obj.saveEmailSettingsFromStorage(emails);
					} else {
						var status = emails[selector];
						$('#' + selector).prop('checked', status);
					}
				});
			}

			function makeHeaderSticky() {
				var header = $('.header-with-nav');
				var nav = $('.nav-primary');
				var breadcrumb = $('.breadcrumb');
				var pointer = $('#sticky-email-header').length;

				if (pointer) {
					header.addClass('sticky-header');
					nav.addClass('sticky-nav');
					breadcrumb.addClass('sticky-layout');
				}
			}

			function getKeyDate(date, days) {
				var resDate = new Date(date);
				resDate.setDate(resDate.getDate() + days);
				return resDate.toISOString().split('T')[0];
			}

			function capitalizeFirstLetter(string) {
				return string.charAt(0).toUpperCase() + string.slice(1);
			}

			function toggleCheckboxAvailability(selector, target) {
				$(selector).change(function () {
					$(target).prop('disabled', function (i, v) {
						return !v;
					});
				}).change();
			}

			function isEmpty(string) {
				return (string.length === 0 || !string.trim());
			}

			function updateDates(start) {
				$('.emailFirstDate').text(getKeyDate(start, 0));
				$('.emailSecondDate').text(getKeyDate(start, 7));
				$('.emailThirdDate').text(getKeyDate(start, 21));
				$('.emailFourDate').text(getKeyDate(start, 35));
				$('.emailFiveDate').text(getKeyDate(start, 45));
			}

			function loadEmailTypeChange(obj, selector, type, partBtn, adminBtn) {
				// Initially hide edit icons
				if (adminBtn !== '#editResultsTemplateBtnAdmin') {
					$(adminBtn).hide();
				}

				// Show edit buttons when select radio
				$('input[name="' + type + '"]').change(function () {
					var part = $('#' + type + '-part').is(':checked');
					var admin = $('#' + type + '-admin').is(':checked');
					if (part === true) {
						$(partBtn).show();
						$(adminBtn).hide();
						$('.' + type + '-introductory-part').show();
						$('.' + type + '-introductory-admin').hide();
						obj.fillBodyText(selector, type,
							'overwriteMessage');
					} else if (admin === true) {
						$(adminBtn).show();
						$(partBtn).hide();
						$('.' + type + '-introductory-part').hide();
						$('.' + type + '-introductory-admin').show();
						obj.fillBodyText(selector, type,
							'adminMessage');
					}
				});
			}

			(function () {
				// Email subject variables
				// Welcome email
				var firstSubjPart = $('#first-donor-subject-part');
				var firstSubjAdm = $('#first-donor-subject-admin');
				var secondSubjPart = $('#second-donor-subject-part');
				var secondSubjAdm = $('#second-donor-subject-admin');
				// Reminder email
				var thirdSubjPart = $('#third-donor-subject-part');
				var thirdSubjAdm = $('#third-donor-subject-admin');
				var fourSubjPart = $('#four-donor-subject-part');
				var fourSubjAdm = $('#four-donor-subject-admin');
				// Initially hide admin letter
				firstSubjAdm.hide();
				secondSubjAdm.hide();
				thirdSubjAdm.hide();
				fourSubjAdm.hide();

				$('input[name="welcomeEmail"]').change(function () {
					var partAnch = $('#welcomeEmail-part').is(':checked');
					var admAnch = $('#welcomeEmail-admin').is(':checked');
					if (partAnch) {
						firstSubjAdm.hide();
						firstSubjPart.show();
						secondSubjAdm.hide();
						secondSubjPart.show();
					} else if (admAnch) {
						firstSubjAdm.show();
						firstSubjPart.hide();
						secondSubjAdm.show();
						secondSubjPart.hide();
					}
				});

				$('input[name="reminderEmail"]').change(function () {
					var partAnch = $('#reminderEmail-part').is(':checked');
					var admAnch = $('#reminderEmail-admin').is(':checked');
					if (partAnch) {
						thirdSubjAdm.hide();
						thirdSubjPart.show();
						fourSubjAdm.hide();
						fourSubjPart.show();
					} else if (admAnch) {
						thirdSubjAdm.show();
						thirdSubjPart.hide();
						fourSubjAdm.show();
						fourSubjPart.hide();
					}
				});

			})();

			(function () {
				// Hide two steps
				$('.results-goal-second').hide();
				$('.results-goal-third').hide();
				$('#resultsTypeSelector').change(function () {
					// We got three statuses:
					// First - if the participant did reach goal and the PRGM did reach goal
					// Second - if the participant did reach goal and the PRGM did not
					// Third - if the participant did not reach goal
					var first = $('#reminderGoalFirst').is(':selected');
					var second = $('#reminderGoalSecond').is(':selected');
					var third = $('#reminderGoalThird').is(':selected');

					var firstBody = $('.results-goal-first');
					var secondBody = $('.results-goal-second');
					var thirdBody = $('.results-goal-third');

					if (first === true) {
						firstBody.show();
						secondBody.hide();
						thirdBody.hide();
					} else if (second === true) {
						firstBody.hide();
						secondBody.show();
						thirdBody.hide();
					} else if (third === true) {
						firstBody.hide();
						secondBody.hide();
						thirdBody.show();
					}
				});
			})();

			// GLOBAL FUNCTIONS FOR A EMAIL PAGE
			(function () {
				var edit = new EmailConfig();

				// Load init values to the localStorage
				// ! We need this only once !
				edit.loadInitToStorage();

				// Update key dates for all email types
				updateKeyDates(edit);

				// Update campaign name
			//	fillCampaignName(edit);

				// Make header sticky
				makeHeaderSticky();

				$('.email-control-paragraph').hide();

				// Toggle accordion click listeners
				edit.openEmail('#openLanding', '#closeLanding', '#landing');
				edit.openEmail('#openEmails', '#closeEmails', '#welcome');
				edit.openEmail('#openReminders', '#closeReminders', '#reminder');
				edit.openEmail('#openResults', '#closeResults', '#results');
				edit.openEmail('#openThank', '#closeThank', '#immediate');
				edit.openEmail('#openPersonal', '#closePersonal', '#personal');
				edit.openEmail('#openScheduled', '#closeScheduled', '#scheduled');

				// Suppress checkboxes click listeners
				edit.suppressEmailChange('suppressFirstDonorDate', edit);
				edit.suppressEmailChange('suppressSecondDonorDate', edit);
				edit.suppressEmailChange('suppressThirdDonorDate', edit);
				edit.suppressEmailChange('suppressFourDonorDate', edit);
				edit.suppressEmailChange('suppressFiveDonorDate', edit);
				edit.suppressEmailChange('suppressPersonal', edit);
				edit.suppressEmailChange('suppressScheduled', edit);

				// Fill email values on date change
				$('#edit-date').on('change', function () {
					var currentValue = $(this).val();
					updateDates(currentValue);
				});

			})();

			// WELCOME CAMPAIGN LETTER
			(function () {
				var welcome = new EmailConfig();
				var editedTarget;
				// Edit / Save / Cancel buttons
				var edit = $('#editWelcomeTemplateBtnPart');
				var editAdmin = $('#editWelcomeTemplateBtnAdmin');
				var cancel = $('#cancelWelcomeTemplateBtn');
				var save = $('#saveWelcomeTemplateBtn');

				// Show edit template section text
				var section = $('.show-welcome-paragraph-body');
				var adminIntroductory = $('.welcomeEmail-introductory-admin');

				// Toggle edit view body (participant or admin template)
				loadEmailTypeChange(welcome, '#bodyMessage', 'welcomeEmail',
					'#editWelcomeTemplateBtnPart', '#editWelcomeTemplateBtnAdmin');

				section.hide();
				adminIntroductory.hide();

				// RTE editor
				var editor = $('#rtc-editor');
				var editorBody = $('.preview-welcome-body-info');

				// Click on hidden button to load text inside RTE
				$('#loadOverwrite').click();

				// Load welcome email toggled
				welcome.togglePreviewSection('#show-introductory', '.welcome-letter-body');
				welcome.togglePreviewSection('#show-payment', '.welcome-payment-methods');
				welcome.togglePreviewSection('#show-closing', '.welcome-closing-paragraph');

				// Toggle hidden welcome email template fields
				toggleWelcomeEdit(welcome);

				// Show or hide welcome email template fields
				// based on localStorage configs
				updateControls(welcome, 'welcomeEmail');

				// Init RTE
				welcome.initRTE('welcomeEmail-email_editor', 'welcomeEmail');

				// Fill initial text to the overwritten body
				welcome.fillBodyText('#bodyMessage', 'welcomeEmail');

				// Edit button function
				function init() {
					toggleWelcomeEdit(welcome);
					welcome.openEmail('direct', '#closeWelcome', '#welcome');
					welcome.fillStartDate('#edit-date');
					welcome.setMinToDateInput('#edit-date');
					section.show();
					editor.hide();
					welcome.updateShowButtons('#show-p1', 'showIntroductory', 'welcomeEmail');
					welcome.updateShowButtons('#show-p2', 'showPayment', 'welcomeEmail');
					welcome.updateShowButtons('#show-p3', 'showClosing', 'welcomeEmail');
				}

				// Edit welcome email template button listener
				edit.click(function () {
					init();
					$(this).toggle();
					editedTarget = 'part';

					welcome.loadRTEContent('welcomeEmail', 'overwriteMessage');

					// Save RTE
					welcome.saveRTEContentListener('#saveOverwrite',
						'welcomeEmail',
						'#bodyMessage',
						'#hidden-rtc-edit-close',
						'overwriteMessage');
				});

				editAdmin.click(function () {

					init();
					$(this).toggle();
					editedTarget = 'admin';

					welcome.loadRTEContent('welcomeEmail', 'adminMessage');

					// Save RTE
					welcome.saveRTEContentListener('#saveOverwrite',
						'welcomeEmail',
						'#bodyMessage',
						'#hidden-rtc-edit-close',
						'adminMessage');
				});

				// Cancel edit welcome template button listener
				cancel.click(function () {
					toggleWelcomeEdit(welcome);
					updateControls(welcome, 'welcomeEmail');
					editor.hide();
					section.hide();
					if (editedTarget === 'part') {
						edit.show();
					} else {
						editAdmin.show();
					}
				});

				// Save edited welcome template
				save.click(function () {
					toggleWelcomeEdit(welcome);
					welcome.saveUpdatedKeyDates();
					updateKeyDates(welcome);
					welcome.saveEditedTemplate(
						'#show-introductory',
						'#show-payment',
						'#show-closing',
						'#show-override-template',
						'welcomeEmail'
					);
					section.hide();
					editor.hide();
					editorBody.show();
					if (editedTarget === 'part') {
						edit.show();
					} else {
						editAdmin.show();
					}
				});

				// Click and hide show button and row
				welcome.reclickAndHide('#show-p1', '#show-introductory');
				welcome.reclickAndHide('#show-p2', '#show-payment');
				welcome.reclickAndHide('#show-p3', '#show-closing');

				// Hide section
				welcome.toggleSection('#hidden-email-preview', '#show-introductory', '.welcome-letter-body', '#show-p1');
				welcome.toggleSection('#togglePaymentMethod', '#show-payment', '.welcome-payment-methods', '#show-p2');
				welcome.toggleSection('#hidden-email-closing-preview', '#show-closing', '.welcome-closing-paragraph', '#show-p3');

				welcome.toggleEditView('#hidden-email-body-edit', '.preview-welcome-body-info',
					'#rtc-editor', '#show-override-template', true);
				welcome.toggleEditView('#hidden-rtc-edit-close', '#rtc-editor',
					'.preview-welcome-body-info', '#show-override-template', false);

				// Update edited text
				welcome.updateEditedText('#updateOverwrite', '#bodyMessage', 'welcomeEmail');

				// Disable send email input when checkbox checked
				toggleCheckboxAvailability('.send-for-you-checkbox1', '#send-test-input1');

			})();

			// REMINDER LETTER
			(function () {
				var reminder = new EmailConfig();
				var editedTarget;

				// Edit / Save / Cancel buttons
				var edit = $('#editReminderTemplateBtnPart');
				var editAdmin = $('#editReminderTemplateBtnAdmin');
				var cancel = $('#cancelReminderTemplateBtn');
				var save = $('#saveReminderTemplateBtn');

				// Show edit template section text
				var section = $('.show-reminder-paragraph-body');

				// Toggle edit view body (participant or admin template)
				loadEmailTypeChange(reminder, '#reminder-bodyMessage', 'reminderEmail',
					'#editReminderTemplateBtnPart', '#editReminderTemplateBtnAdmin');

				section.hide();

				// RTC editor
				var editor = $('#reminder-rtc-editor');
				var editorBody = $('.preview-reminder-body-info');


				// Click on hidden button to load text inside RTE
				$('#reminder-loadOverwrite').click();

				// Load welcome email toggled
				reminder.togglePreviewSection('#reminder-show-introductory', '.reminder-body');
				reminder.togglePreviewSection('#reminder-show-payment', '.reminder-payment-methods');
				reminder.togglePreviewSection('#reminder-show-closing', '.reminder-closing-paragraph');

				// Toggle hidden welcome email template fields
				toggleEmailEdit(reminder, 'reminder');

				// Init RTE
				reminder.initRTE('reminderEmail-email_editor', 'reminderEmail');

				// Show or hide welcome email template fields
				// based on localStorage configs
				updateEmailControls(reminder, 'reminderEmail', 'reminder');

				// Fill initial text to the overwritten body
				reminder.fillBodyText('#reminder-bodyMessage', 'reminderEmail');

				function init() {
					toggleEmailEdit(reminder, 'reminder');
					reminder.openEmail('direct', '#closeReminder', '#reminder');
					section.show();
					editor.hide();
					reminder.updateShowButtons('#reminder-show-p1', 'showIntroductory', 'reminderEmail');
					reminder.updateShowButtons('#reminder-show-p2', 'showPayment', 'reminderEmail');
					reminder.updateShowButtons('#reminder-show-p3', 'showClosing', 'reminderEmail');
				}

				// Edit welcome email template button listener
				edit.click(function () {

					init();
					$(this).toggle();
					editedTarget = 'part';

					reminder.loadRTEContent('reminderEmail', 'overwriteMessage');

					// Save RTE
					reminder.saveRTEContentListener('#reminder-saveOverwrite',
						'reminderEmail',
						'#reminder-bodyMessage',
						'#reminder-hidden-rtc-edit-close',
						'overwriteMessage');
				});

				editAdmin.click(function () {

					init();
					$(this).toggle();
					editedTarget = 'admin';

					reminder.loadRTEContent('reminderEmail', 'adminMessage');

					// Save RTE
					reminder.saveRTEContentListener('#reminder-saveOverwrite',
						'reminderEmail',
						'#reminder-bodyMessage',
						'#reminder-hidden-rtc-edit-close',
						'adminMessage');

				});

				// Cancel edit welcome template button listener
				cancel.click(function () {
					toggleEmailEdit(reminder, 'reminder');
					if (editedTarget === 'part') {
						edit.show();
					} else {
						editAdmin.show();
					}
					updateEmailControls(reminder, 'reminderEmail', 'reminder');
					editor.hide();
					section.hide();
				});

				// Save edited welcome template
				save.click(function () {
					toggleEmailEdit(reminder, 'reminder');
					reminder.saveEditedTemplate(
						'#reminder-show-introductory',
						'#reminder-show-payment',
						'#reminder-show-closing',
						'#reminder-show-override-template',
						'reminderEmail'
					);
					if (editedTarget === 'part') {
						edit.show();
					} else {
						editAdmin.show();
					}
					section.hide();
					editor.hide();
					editorBody.show();
				});

				// Click and hide show button and row
				reminder.reclickAndHide('#reminder-show-p1', '#reminder-show-introductory');
				reminder.reclickAndHide('#reminder-show-p2', '#reminder-show-payment');
				reminder.reclickAndHide('#reminder-show-p3', '#reminder-show-closing');

				// Hide section
				reminder.toggleSection('#reminder-hidden-email-preview', '#reminder-show-introductory',
					'.reminder-body', '#reminder-show-p1');
				reminder.toggleSection('#reminder-togglePaymentMethod', '#reminder-show-payment',
					'.reminder-payment-methods', '#reminder-show-p2');
				reminder.toggleSection('#reminder-hidden-email-closing-preview', '#reminder-show-closing',
					'.reminder-closing-paragraph', '#reminder-show-p3');

				reminder.toggleEditView('#reminder-hidden-email-body-edit', '.preview-reminder-body-info',
					'#reminder-rtc-editor', '#reminder-show-override-template', true);
				reminder.toggleEditView('#reminder-hidden-rtc-edit-close', '#reminder-rtc-editor',
					'.preview-reminder-body-info', '#reminder-show-override-template', false);

				// Update edited text
				reminder.updateEditedText('#reminder-updateOverwrite', '#reminder-bodyMessage', 'reminderEmail');

				// Disable send email input when checkbox checked
				toggleCheckboxAvailability('.send-for-you-checkbox2', '#send-test-input2');

			})();

			// RESULTS LETTER
			(function () {
				var results = new EmailConfig();
				var editedTarget;
				// Edit / Save / Cancel buttons

				var editAdmin = $('#editResultsTemplateBtnAdmin');
				var cancel = $('#cancelResultsTemplateBtn');
				var save = $('#saveResultsTemplateBtn');

				// Show edit template section text
				var section = $('.show-results-paragraph-body');

				// Toggle edit view body (participant or admin template)
				loadEmailTypeChange(results, '#results-bodyMessage', 'resultsEmail',
					'#editResultsTemplateBtnPart', '#editResultsTemplateBtnAdmin');

				section.hide();

				// RTE editor
				var editor = $('#results-rtc-editor');
				var editorBody = $('.preview-results-body-info');

				// Click on hidden button to load text inside RTE
				$('#results-loadOverwrite').click();

				// Load welcome email toggled
				results.togglePreviewSection('#results-show-introductory', '.results-body');
				results.togglePreviewSection('#results-show-payment', '.results-payment-methods');
				results.togglePreviewSection('#results-show-closing', '.results-closing-paragraph');

				// Toggle hidden welcome email template fields
				toggleEmailEdit(results, 'results');

				// Init RTE
				// results.initRTE('resultsEmail-email_editor', 'reminderEmail');

				// Show or hide welcome email template fields
				// based on localStorage configs
				updateEmailControls(results, 'resultsEmail', 'results');

				// Fill initial text to the overwritten body
				results.fillBodyText('#results-bodyMessage', 'resultsEmail');

				function init() {
					toggleEmailEdit(results, 'results');
					results.openEmail('direct', '#closeResults', '#results');
					section.show();
					editor.hide();
					results.updateShowButtons('#results-show-p1', 'showIntroductory', 'resultsEmail');
					results.updateShowButtons('#results-show-p2', 'showPayment', 'resultsEmail');
					results.updateShowButtons('#results-show-p3', 'showClosing', 'resultsEmail');
				}

				// Edit welcome email template button listener

				editAdmin.click(function () {

					$(this).toggle();
					init();
					editedTarget = 'admin';

					results.loadRTEContent('resultsEmail', 'adminMessage');

					// Save RTE
					results.saveRTEContentListener('#results-saveOverwrite',
						'resultsEmail',
						'#results-bodyMessage',
						'#results-hidden-rtc-edit-close',
						'adminMessage');
				});

				// Cancel edit welcome template button listener
				cancel.click(function () {
					toggleEmailEdit(results, 'results');
					if (editedTarget === 'part') {

					} else {
						editAdmin.show();
					}
					updateEmailControls(results, 'resultsEmail', 'results');
					editor.hide();
					section.hide();
				});

				// Save edited welcome template
				save.click(function () {
					toggleEmailEdit(results, 'results');
					results.saveEditedTemplate(
						'#results-show-introductory',
						'#results-show-payment',
						'#results-show-closing',
						'#results-show-override-template',
						'resultsEmail'
					);
					if (editedTarget === 'part') {

					} else {
						editAdmin.show();
					}
					section.hide();
					editor.hide();
					editorBody.show();
				});

				// Click and hide show button and row
				results.reclickAndHide('#results-show-p1', '#results-show-introductory');
				results.reclickAndHide('#results-show-p2', '#results-show-payment');
				results.reclickAndHide('#results-show-p3', '#results-show-closing');

				// Hide section
				results.toggleSection('#results-hidden-email-preview', '#results-show-introductory',
					'.results-body', '#results-show-p1');
				results.toggleSection('#results-togglePaymentMethod', '#results-show-payment',
					'.results-payment-methods', '#results-show-p2');
				results.toggleSection('#results-hidden-email-closing-preview', '#results-show-closing',
					'.results-closing-paragraph', '#results-show-p3');

				results.toggleEditView('#results-hidden-email-body-edit', '.preview-results-body-info',
					'#results-rtc-editor', '#results-show-override-template', true);
				results.toggleEditView('#results-hidden-rtc-edit-close', '#results-rtc-editor',
					'.preview-results-body-info', '#results-show-override-template', false);

				// Update edited text
				results.updateEditedText('#results-updateOverwrite', '#results-bodyMessage', 'resultsEmail');

				// Disable send email input when checkbox checked
				toggleCheckboxAvailability('.send-for-you-checkbox3', '#send-test-input3');

			})();

			(function () {
				var immediate = new EmailConfig();
				var editedTarget;

				// Edit / Save / Cancel buttons
				var edit = $('#editImmediateTemplateBtnPart');
				var cancel = $('#cancelImmediateTemplateBtn');
				var save = $('#saveImmediateTemplateBtn');

				// Show edit template section text
				var section = $('.show-immediate-paragraph-body');

				// Toggle edit view body (participant or admin template)
				loadEmailTypeChange(immediate, '#immediate-bodyMessage', 'immediateEmail',
					'#editImmediateTemplateBtnPart', '#editImmediateTemplateBtnAdmin');

				section.hide();

				// RTE editor
				var editor = $('#immediate-rtc-editor');
				var editorBody = $('.preview-immediate-body-info');

				// Init RTE
				immediate.initRTE('immediateEmail-email_editor', 'immediateEmail');


				// Click on hidden button to load text inside RTE
				$('#immediate-loadOverwrite').click();

				// Load welcome email toggled
				immediate.togglePreviewSection('#immediate-show-introductory', '.immediate-body');
				immediate.togglePreviewSection('#immediate-show-payment', '.immediate-payment-methods');
				immediate.togglePreviewSection('#immediate-show-closing', '.immediate-closing-paragraph');

				// Toggle hidden welcome email template fields
				toggleEmailEdit(immediate, 'immediate');

				// Show or hide welcome email template fields
				// based on localStorage configs
				updateEmailControls(immediate, 'immediateEmail', 'immediate');

				// Fill initial text to the overwritten body
				immediate.fillBodyText('#immediate-bodyMessage', 'immediateEmail');

				function init() {
					toggleEmailEdit(immediate, 'immediate');

					immediate.openEmail('direct', '#closeImmediate', '#immediate');
					section.show();
					editor.hide();
					immediate.updateShowButtons('#immediate-show-p1', 'showIntroductory', 'immediateEmail');
					immediate.updateShowButtons('#immediate-show-p2', 'showPayment', 'immediateEmail');
					immediate.updateShowButtons('#immediate-show-p3', 'showClosing', 'immediateEmail');
				}

				// Edit welcome email template button listener
				edit.click(function () {

					init();
					$(this).toggle();
					editedTarget = 'part';

					immediate.loadRTEContent('immediateEmail', 'overwriteMessage');

					// Save RTE
					immediate.saveRTEContentListener('#immediate-saveOverwrite',
						'immediateEmail',
						'#immediate-bodyMessage',
						'#immediate-hidden-rtc-edit-close',
						'overwriteMessage');
				});

				// Cancel edit welcome template button listener
				cancel.click(function () {
					toggleEmailEdit(immediate, 'immediate');
					if (editedTarget === 'part') {
						edit.show();
					} else {
					}
					updateEmailControls(immediate, 'immediateEmail', 'immediate');
					editor.hide();
					section.hide();
				});

				// Save edited welcome template
				save.click(function () {
					toggleEmailEdit(immediate, 'immediate');
					immediate.saveEditedTemplate(
						'#immediate-show-introductory',
						'#immediate-show-payment',
						'#immediate-show-closing',
						'#immediate-show-override-template',
						'immediateEmail'
					);
					if (editedTarget === 'part') {
						edit.show();
					} else {
					}
					section.hide();
					editor.hide();
					editorBody.show();
				});

				// Click and hide show button and row
				immediate.reclickAndHide('#immediate-show-p1', '#immediate-show-introductory');
				immediate.reclickAndHide('#immediate-show-p2', '#immediate-show-payment');
				immediate.reclickAndHide('#immediate-show-p3', '#immediate-show-closing');

				// Hide section
				immediate.toggleSection('#immediate-hidden-email-preview', '#immediate-show-introductory',
					'.immediate-body', '#immediate-show-p1');
				immediate.toggleSection('#immediate-togglePaymentMethod', '#immediate-show-payment',
					'.immediate-payment-methods', '#immediate-show-p2');
				immediate.toggleSection('#immediate-hidden-email-closing-preview', '#immediate-show-closing',
					'.immediate-closing-paragraph', '#immediate-show-p3');

				immediate.toggleEditView('#immediate-hidden-email-body-edit', '.preview-immediate-body-info',
					'#immediate-rtc-editor', '#immediate-show-override-template', true);
				immediate.toggleEditView('#immediate-hidden-rtc-edit-close', '#immediate-rtc-editor',
					'.preview-immediate-body-info', '#immediate-show-override-template', false);

				// Update edited text
				immediate.updateEditedText('#immediate-updateOverwrite', '#immediate-bodyMessage', 'immediateEmail');

				// Disable send email input when checkbox checked
				toggleCheckboxAvailability('.send-for-you-checkbox4', '#send-test-input4');

			})();

			(function () {
				var personal = new EmailConfig();
				var editedTarget;

				// Edit / Save / Cancel buttons
				var edit = $('#editPersonalTemplateBtnPart');
				var editAdmin = $('#editPersonalTemplateBtnAdmin');
				var cancel = $('#cancelPersonalTemplateBtn');
				var save = $('#savePersonalTemplateBtn');

				// Show edit template section text
				var section = $('.show-personal-paragraph-body');

				// Toggle edit view body (participant or admin template)
				loadEmailTypeChange(personal, '#personal-bodyMessage', 'personalEmail',
					'#editPersonalTemplateBtnPart', '#editPersonalTemplateBtnAdmin');

				section.hide();

				// RTE editor
				var editor = $('#personal-rtc-editor');
				var editorBody = $('.preview-personal-body-info');

				// Init RTE
				personal.initRTE('personalEmail-email_editor', 'personalEmail');

				// Click on hidden button to load text inside RTE
				$('#personal-loadOverwrite').click();

				// Load welcome email toggled
				personal.togglePreviewSection('#personal-show-introductory', '.personal-body');
				personal.togglePreviewSection('#personal-show-payment', '.personal-payment-methods');
				personal.togglePreviewSection('#personal-show-closing', '.personal-closing-paragraph');

				// Toggle hidden welcome email template fields
				toggleEmailEdit(personal, 'personal');

				// Show or hide welcome email template fields
				// based on localStorage configs
				updateEmailControls(personal, 'personalEmail', 'personal');
				// Fill initial text to the overwritten body
				personal.fillBodyText('#personal-bodyMessage', 'personalEmail');

				function init() {
					toggleEmailEdit(personal, 'personal');
					personal.openEmail('direct', '#closePersonal', '#personal');
					section.show();
					editor.hide();
					personal.updateShowButtons('#personal-show-p1', 'showIntroductory', 'personalEmail');
					personal.updateShowButtons('#personal-show-p2', 'showPayment', 'personalEmail');
					personal.updateShowButtons('#personal-show-p3', 'showClosing', 'personalEmail');
				}

				// Edit welcome email template button listener
				edit.click(function () {

					init();
					$(this).toggle();
					editedTarget = 'part';

					personal.loadRTEContent('personalEmail', 'overwriteMessage');
					// Save RTE
					personal.saveRTEContentListener('#personal-saveOverwrite',
						'personalEmail',
						'#personal-bodyMessage',
						'#personal-hidden-rtc-edit-close',
						'overwriteMessage');
				});

				editAdmin.click(function () {

					init();
					$(this).toggle();
					editedTarget = 'admin';

					personal.loadRTEContent('personaEmail', 'adminMessage');
					// Save RTE
					personal.saveRTEContentListener('#personal-saveOverwrite',
						'personalEmail',
						'#personal-bodyMessage',
						'#personal-hidden-rtc-edit-close',
						'adminMessage');
				});

				// Cancel edit welcome template button listener
				cancel.click(function () {
					toggleEmailEdit(personal, 'personal');
					edit.toggle();
					updateEmailControls(personal, 'personalEmail', 'personal');
					editor.hide();
					section.hide();
				});

				// Save edited welcome template
				save.click(function () {
					toggleEmailEdit(personal, 'personal');
					personal.saveEditedTemplate(
						'#personal-show-introductory',
						'#personal-show-payment',
						'#personal-show-closing',
						'#personal-show-override-template',
						'personalEmail'
					);
					if (editedTarget === 'part') {
						edit.show();
					} else {
						editAdmin.show();
					}
					section.hide();
					editor.hide();
					editorBody.show();
				});

				// Click and hide show button and row
				personal.reclickAndHide('#personal-show-p1', '#personal-show-introductory');
				personal.reclickAndHide('#personal-show-p2', '#personal-show-payment');
				personal.reclickAndHide('#personal-show-p3', '#personal-show-closing');

				// Hide section
				personal.toggleSection('#personal-hidden-email-preview', '#personal-show-introductory',
					'.personal-body', '#personal-show-p1');
				personal.toggleSection('#personal-togglePaymentMethod', '#personal-show-payment',
					'.personal-payment-methods', '#personal-show-p2');
				personal.toggleSection('#personal-hidden-email-closing-preview', '#personal-show-closing',
					'.personal-closing-paragraph', '#personal-show-p3');

				personal.toggleEditView('#personal-hidden-email-body-edit', '.preview-personal-body-info',
					'#personal-rtc-editor', '#personal-show-override-template', true);
				personal.toggleEditView('#personal-hidden-rtc-edit-close', '#personal-rtc-editor',
					'.preview-personal-body-info', '#personal-show-override-template', false);

				// Update edited text
				personal.updateEditedText('#personal-updateOverwrite', '#personal-bodyMessage', 'personalEmail');

				// Disable send email input when checkbox checked
				toggleCheckboxAvailability('.send-for-you-checkbox5', '#send-test-input5');

			})();

			(function () {
				var scheduled = new EmailConfig();
				var editedTarget;
				var isEditOn = $('.is-edit-on');
				var isEditOff = $('.is-edit-off');
				// Edit / Save / Cancel buttons
				var edit = $('#editScheduledTemplateBtnPart');
				var editAdmin = $('#editScheduledTemplateBtnAdmin');
				var cancel = $('#cancelScheduledTemplateBtn');
				var save = $('#saveScheduledTemplateBtn');

				// Show edit template section text
				var section = $('.show-scheduled-paragraph-body');

				// Toggle edit view body (participant or admin template)
				loadEmailTypeChange(scheduled, '#scheduled-bodyMessage', 'scheduledEmail',
					'#editScheduledTemplateBtnPart', '#editScheduledTemplateBtnAdmin');
				$('#hide-personalized-intro-btn').on('click', function() {
					$('.personalized-intro-show').hide();
					$('#personalized-intro-hide').show();
				});
				$('#personalized-intro-hide').on('click', function() {
					$('.personalized-intro-show').show();
					$('#personalized-intro-hide').hide();
				});
				$('#show-donate-btn').on('click', function() {
					$('#show-donate-btn').hide();
					$('.donate-btn').show();
				});
				$('#hide-donate-btn').on('click', function() {
					$('.donate-btn').hide();
					$('#show-donate-btn').show();
				});
				section.hide();
				isEditOn.hide();
				isEditOff.show();
				// RTE editor
				var editor = $('#scheduled-rtc-editor');
				var editorBody = $('.preview-scheduled-body-info');

				// Init RTE
				scheduled.initRTE('scheduledEmail-email_editor', 'scheduledEmail');

				// Click on hidden button to load text inside RTE
				$('#scheduled-loadOverwrite').click();

				// Load welcome email toggled
				scheduled.togglePreviewSection('#scheduled-show-introductory', '.scheduled-body');
				scheduled.togglePreviewSection('#scheduled-show-payment', '.scheduled-payment-methods');
				scheduled.togglePreviewSection('#scheduled-show-closing', '.scheduled-closing-paragraph');

				// Toggle hidden welcome email template fields
				toggleEmailEdit(scheduled, 'scheduled');

				// Show or hide welcome email template fields
				// based on localStorage configs
			//	updateEmailControls(scheduled, 'scheduledEmail', 'scheduled');
				// Fill initial text to the overwritten body
			//	scheduled.fillBodyText('#scheduled-bodyMessage', 'scheduledEmail');
			//	scheduled.loadRTEContent('scheduledEmail', 'overwriteMessage');
				// Toggle personalized
				$('#scheduled-show-personalized').click(function(){
					$('.welcome-scheduled-title').toggle();
				});

				function init() {
					toggleEmailEdit(scheduled, 'scheduled');
					scheduled.openEmail('direct', '#closeScheduled', '#scheduled');
					section.show();
					editor.hide();
				}

				// Edit welcome email template button listener
				edit.click(function () {
					isEditOff.hide();
					isEditOn.show();
					init();
					$(this).toggle();
					editedTarget = 'part';

				//	scheduled.loadRTEContent('scheduledEmail', 'overwriteMessage');
					// Save RTE
					scheduled.saveRTEContentListener('#scheduled-saveOverwrite',
						'scheduledEmail',
						'#scheduled-bodyMessage',
						'#scheduled-hidden-rtc-edit-close',
						'overwriteMessage');
				});

				editAdmin.click(function () {

					init();
					$(this).toggle();
					editedTarget = 'admin';

					scheduled.loadRTEContent('scheduledEmail', 'adminMessage');
					// Save RTE
					scheduled.saveRTEContentListener('#scheduled-saveOverwrite',
						'scheduledEmail',
						'#scheduled-bodyMessage',
						'#scheduled-hidden-rtc-edit-close',
						'adminMessage');
				});

				// Cancel edit welcome template button listener
				cancel.click(function () {
					isEditOn.hide();
					isEditOff.show();
					toggleEmailEdit(scheduled, 'scheduled');
					edit.toggle();
					// updateEmailControls(scheduled, 'scheduledEmail', 'scheduled');
					editor.hide();
					section.hide();
					$('#show-donate-btn').hide();
				});

				// Save edited welcome template
				save.click(function () {
					isEditOn.hide();
					isEditOff.show();
					$('#show-donate-btn').hide();
					toggleEmailEdit(scheduled, 'scheduled');
					// scheduled.saveEditedTemplate(
					// 	'#scheduled-show-introductory',
					// 	'#scheduled-show-payment',
					// 	'#scheduled-show-closing',
					// 	'#scheduled-show-override-template',
					// 	'scheduledEmail'
					// );
					if (editedTarget === 'part') {
						edit.show();
					} else {
						editAdmin.show();
					}
					section.hide();
					editor.hide();
					editorBody.show();
				});

				// Click and hide show button and row
				scheduled.reclickAndHide('#scheduled-show-p1', '#scheduled-show-introductory');
				scheduled.reclickAndHide('#scheduled-show-p2', '#scheduled-show-payment');
				scheduled.reclickAndHide('#scheduled-show-p3', '#scheduled-show-closing');

				// Hide section
				scheduled.toggleSection('#scheduled-hidden-email-preview', '#scheduled-show-introductory',
					'.scheduled-body', '#scheduled-show-p1');
				scheduled.toggleSection('#scheduled-togglePaymentMethod', '#scheduled-show-payment',
					'.scheduled-payment-methods', '#scheduled-show-p2');
				scheduled.toggleSection('#scheduled-hidden-email-closing-preview', '#scheduled-show-closing',
					'.scheduled-closing-paragraph', '#scheduled-show-p3');

				scheduled.toggleEditView('#scheduled-hidden-email-body-edit', '.preview-scheduled-body-info',
					'#scheduled-rtc-editor', '#scheduled-show-override-template', true);
				scheduled.toggleEditView('#scheduled-hidden-rtc-edit-close', '#scheduled-rtc-editor',
					'.preview-scheduled-body-info', '#scheduled-show-override-template', false);

				// Update edited text
			//	scheduled.updateEditedText('#scheduled-updateOverwrite', '#scheduled-bodyMessage', 'scheduledEmail');

				// Disable send email input when checkbox checked
			//	toggleCheckboxAvailability('.send-for-you-checkbox5', '#send-test-input6');

			})();
		}
	}
});

