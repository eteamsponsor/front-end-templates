var $ = require('jquery');
require('../../../components/carousel');

$(document).ready(function() {
	$('.carousel').carousel({
		interval: false
	});
	$('#carousel-programs-mobile').carousel({
		interval: false
	});
});
