var $ = require('jquery');
require('../../../components/jquery.inputmask');

$(document).ready(function () {

	if (top.location.pathname === '/fundraker/campaign-edit.html') {
		var anchor = $('#campaign-edit-template');
		if (anchor.length > 0) {
			// Check for a campaign status
			(function () {
				var campaign = getCampaignFromStorage();
				// Get finished percentage
				if (campaign !== null) {
					if (campaign.unfinishedStepValue != null) {
						var status = campaign.unfinishedStepValue;
						var launchButtonRow = $('.preview-button-row');
						if (status === 100) {
							launchButtonRow.show()
						} else {
							launchButtonRow.hide()
						}
					}
				}
			})();

			/*
            BUTTON LISTENERS
             */

			// Load campaign name
			loadCampaignPageInfo();

			function loadCampaignPageInfo() {
				var localCampaign = getCampaignFromStorage();
				if (localCampaign !== null) {
					var cname = $('#ecampaignName');
					var cpart = $('#ecampaignParc');
					var cpartgoal = $('#ecampaignParcGoal');
					var campgoal = $('#ecampaignGoal');
					var vimeo = $('#vimeoId');

					// Email builder
					var emailcname = $('.preview-email-campaign-name');
					var emailpname = $('.preview-email-title-text');
					var emailpgoal = $('.preview-email-personal-goal');

					cname.val(localCampaign.campaignName);
					cpart.val(localCampaign.expectedParticipantPercent);
					cpartgoal.val(localCampaign.participantGoal);
					campgoal.val(localCampaign.campaignGoal);
					vimeo.val(localCampaign.teamVideo);

					emailcname.text(localCampaign.campaignName);
					emailpname.text(localCampaign.programName);
					emailpgoal.text(localCampaign.participantGoal);
				}
			}

			// Get campaign from storage
			function getCampaignFromStorage() {
				var campaignObject = localStorage.getItem('campaign');
				return JSON.parse(campaignObject);
			}
		}
	}
});
