var $ = require('jquery');
require('../../../components/jquery.inputmask');
require('../../../components/jquery.scrollTo.min');
require('../../../components/jquery.duplicate.min');

var programDashCamp = $('#program-dashboard-campaigns');

if (top.location.pathname === '/fundraker/program-dashboard.html') {
	if (programDashCamp.length > 0) {

		var CampaignWizard = function () {

			// Constants and init steps
			var completed = false;
			var isScroll = false;

			var TOTAL_STEPS = 9;
			var PARTICIPANTS = 10;
			var TOTAL_WIZARD_STEPS = 15;

			var currentStep = 1;

			var wizardBody = $('.wizard-modal-body');

			var campaignInit = {
				// Create New Campaign Wizard Steps
				programName: null,
				launchDate: null,
				campaignName: null,
				campaignGoal: null,

				// Edit newly created Campaign
				showGoalOnLanding: false,
				expectedParticipantPercent: null,
				participantGoal: null,
				showGoalOnParticipantsLanding: false,
				campaignNeeds: null,
				additionalNeeds: [],
				campaignKeyDates: null,
				teamVideo: null,
				landingPageMessage: {
					"ops": [
						{
							"insert": "This is super default landing page message. \n"
						}
					]
				},

				// Missing campaign info
				updateProgressPercentage: 0,
				updateUnfinishedTitle: '',
				updateFinishedStep: 0
			};

			var campaignSteps = {
				showGoalOnLanding: 'Goal Visibility',
				expectedParticipantPercent: 'Expected Participation Percentage',
				participantGoal: 'Participant Goal',
				expectedParticipantGoal: 'Expected Participation Percentage',
				showGoalOnParticipantsLanding: 'Show Goal on Participants\' Landing Page',
				campaignNeeds: 'Campaign Needs',
				campaignKeyDates: 'Key Dates',
				teamVideo: 'Team Video',
				landingPageMessage: 'Landing Page Message'
			};

			this.loadInitToStorage = function () {
				var campaign = this.getCampaignFromStorage();
				if (!campaign) {
					localStorage.setItem('campaign', JSON.stringify(campaignInit));
				} else if (campaign === 'undefined') {
					localStorage.clear();
				} else {
					console.log('Local Storage for a Campaign: We good.')
				}
			};

			this.updateCampaignRow = function () {

				// Get campaign from a local storage
				var campaign = this.getCampaignFromStorage();
				var reviewCampaignButtons = $('.preview-button-row');
				var row = $('#newCampaignRow');
				if (campaign !== null) {

					if (campaign.campaignGoal !== null) {
						row.show();
					} else {
						row.hide();
					}

					// Show preview buttons if campaign is done
					if (campaign.updateProgressPercentage === 100) {
						reviewCampaignButtons.show();
						completed = true;
					} else {
						reviewCampaignButtons.hide();
						completed = false;
					}

					// Preview row selectors
					var title = $('#newCampaignLink'),
						// Which step user miss
						unfinishedTitle = $('#campaignUnfinishedTitle'),
						finishedTitle = $('#campaignMetric'),
						unfinishedBar = $('#campaignProgressBar'),
						participationTitle = $('#campaignParticipationMetric'),
						launchDay = $('#campaignLaunchDay'),
						emailLaunchDay = $('#campaignEmailDay');

					var campaignUnfinishedTitle = campaign.updateUnfinishedTitle;
					title.html(campaign.campaignName || 'New campaign');
					unfinishedTitle.html(campaignUnfinishedTitle);
					finishedTitle.html(campaign.updateFinishedStep + ' of ' + TOTAL_STEPS || '0' + ' of ' + TOTAL_STEPS);
					unfinishedBar.attr('data-value', campaign.updateProgressPercentage + '%');
					unfinishedBar.css('width', campaign.updateProgressPercentage + '%');
					participationTitle.html('0 of 0');
					var campaignLaunchDay = new Date(campaign.launchDate);
					var month = campaignLaunchDay.toLocaleString('en-us', {month: "short"});
					launchDay.html(month + '. ' + campaignLaunchDay.getDate());
					emailLaunchDay.html(month + '. ' + campaignLaunchDay.getDate());
					if (campaign.campaignKeyDates) {
						var emailLaunchDate = new Date(campaign.campaignKeyDates.emailCampaignBeginsDate);
						var emailMonth = emailLaunchDate.toLocaleString('en-us', {month: "short"});
						emailLaunchDay.html(emailMonth + '. ' + emailLaunchDate.getDate());
					}
				} else {
					return false;
				}
			};

			this.loadCampaignInfo = function () {
				var localCampaign = this.getCampaignFromStorage();

				if (localCampaign) {
					var launchDate = $('#wizardDate');
					var campaignName = $('#wizardCampaignName');
					var campaignGoal = $('#wizardCampaignGoal');

					var expectedParticipantPercent = $('#expectPartPer');
					var participantGoal = $('#participantGoal');

					var teamVideo = $('#vimeoVideo');

					launchDate.val(localCampaign.launchDate);
					campaignName.val(localCampaign.campaignName);
					campaignGoal.val(localCampaign.campaignGoal || 10000);

					expectedParticipantPercent.val(localCampaign.expectedParticipantPercent || 100);
					participantGoal.val(localCampaign.participantGoal || 100);

					if (localCampaign.campaignNeeds !== null) {
						var needs = localCampaign.campaignNeeds;
						for (var i = 0; i < needs.length + 1; i++) {
							var currentNeeds = $('input[name=campaign-needs-checkbox][value="' + needs[i] + '"]');
							currentNeeds.prop('checked', 'true');
						}
					}

					if (localCampaign.additionalNeeds !== []) {
						var additionalNeeds = $('.additional-input-field');
						additionalNeeds.val(localCampaign.additionalNeeds[0])
					}

					$('#disabledLaunchDate').val(localCampaign.launchDate);

					$('#ciDate').val(localCampaign.campaignKeyDates.campaignInstructionDate);
					$('#ecbDate').val(localCampaign.campaignKeyDates.emailCampaignBeginsDate);
					$('#eventDate').val(localCampaign.campaignKeyDates.campaignEventDate);
					$('#et2Date').val(localCampaign.campaignKeyDates.emailTouch2Date);
					$('#et3Date').val(localCampaign.campaignKeyDates.emailTouch3Date);
					$('#et4Date').val(localCampaign.campaignKeyDates.emailTouch4Date);
					$('#et5Date').val(localCampaign.campaignKeyDates.emailTouch5Date);

					teamVideo.val(localCampaign.teamVideo);

				}
			};

			this.getCampaignFromStorage = function () {
				// Gets campaign from the local storage and return parsed JSON
				var campaignObject = localStorage.getItem('campaign');
				return JSON.parse(campaignObject);
			};

			this.saveCampaignToStorage = function (obj) {
				localStorage.setItem('campaign', JSON.stringify(obj));
			};

			this.updateProgressBar = function (step) {
				var progressBar = $('.progress-bar');
				if (completed === false) {
					var percent = (step / TOTAL_WIZARD_STEPS) * 100;
					progressBar.text(step + "/ " + TOTAL_WIZARD_STEPS);
					progressBar.css({width: percent + '%'});
				} else {
					progressBar.text(TOTAL_WIZARD_STEPS + " / " + TOTAL_WIZARD_STEPS);
					progressBar.css({width: '100%'});
					progressBar.css('background-color', '#3ab44a');
				}
			};

			this.maskInputNumber = function (selector) {
				$(selector).keypress(function (event) {
					if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
						event.preventDefault();
					}
				}).inputmask("numeric", {
					radixPoint: ".",
					groupSeparator: ",",
					digits: 2,
					autoGroup: true,
					rightAlign: false
				});
			};

			this.maskLink = function (selector) {
				$(selector).inputmask("number", {
					mask: "https://vimeo.com/*{1,9}",
					definitions: {
						'*': {
							validator: "[0-9]"
						}
					}
				});
			};

			this.scrollNextStep = function (direction, updateProgress) {
				isScroll = true;
				if (currentStep < 1) {
					currentStep = 1;
				} else if (currentStep > TOTAL_WIZARD_STEPS) {
					currentStep = TOTAL_WIZARD_STEPS;
				}
				if (direction === 'down') {
					currentStep = currentStep + 1;
					wizardBody.scrollTo('#step' + currentStep, '1000');
					if (currentStep <= TOTAL_WIZARD_STEPS && updateProgress === true) {
						this.updateProgressBar(currentStep);
					}
				} else if (direction === 'up') {
					currentStep = currentStep - 1;
					wizardBody.scrollTo('#step' + currentStep, '1000');
					if (currentStep >= 1 && updateProgress === true) {
						this.updateProgressBar(currentStep);
					}
				}
				setTimeout(function () {
					isScroll = false;
				}, 1000);
			};

			this.clearCampaign = function (selector) {
				$(selector).click(function () {
					var deleteChallenge = confirm("You want to delete this campaign. Are you sure?");
					if (deleteChallenge === true) {
						localStorage.clear();
						location.reload();
					} else {
						return false;
					}
				});
			};

			this.newCampaignClick = function (selector) {
				var self = this;
				$(selector).click(function () {
					localStorage.clear();
					self.loadInitToStorage();
					$('#newCampaignRow').hide();
					wizardBody.scrollTo('#step1', '1000');
					currentStep = 1;
					completed = false;
					self.updateProgressBar(1);
					setTimeout(function () {
						$('#step1').addClass('next-wizard-item');
						$('#step2').removeClass('next-wizard-item');
					}, '300')
				});
			};

			this.editCampaignClick = function (selector) {
				var self = this;
				$(selector).click(function () {
					self.loadCampaignInfo();
					self.gotoUnfinished();
				});
			};

			this.calculateGoal = function () {
				var campaign = this.getCampaignFromStorage();
				var participantCount = $('#participantCount');
				var participantProgramName = $('#participantProgramName');
				var participantCampaign = $('#participantCampaign');
				var participantGoalDesk = $('#participantGoalDesk');

				participantCount.text(PARTICIPANTS);
				participantProgramName.html(' ' + campaign.programName);
				participantCampaign.html(campaign.campaignGoal);

				var participantGoal = ((parseFloat(campaign.campaignGoal) * 1000) / 10);
				participantGoalDesk.html(participantGoal);
				$('#participantGoal').val(participantGoal);
			};

			this.setMinDate = function (selector) {
				var today = new Date().toISOString().split('T')[0];
				$(selector).attr('min', today);
			};

			// Usually used with a 7, 21, 35, 45 days value
			// 7d - first reminder email will be send in 7 days after start.
			// 21, 32, 45 - days after campaign start, when reminder emails
			// will be send
			this.getKeyDate = function (date, days) {
				var date_val = new Date(date);
				date_val.setDate(date_val.getDate() + days);
				return date_val.toISOString().split('T')[0];
			};

			this.setCampaignKeyDates = function (date) {
				var campaign = this.getCampaignFromStorage();
				var campaignKeyDates = {
					campaignInstructionDate: date,
					emailCampaignBeginsDate: date,
					campaignEventDate: date,
					emailTouch2Date: this.getKeyDate(date, 7),
					emailTouch3Date: this.getKeyDate(date, 21),
					emailTouch4Date: this.getKeyDate(date, 35),
					emailTouch5Date: this.getKeyDate(date, 45)
				};
				campaign.campaignKeyDates = campaignKeyDates;
				return campaign.campaignKeyDates;
			};

			this.scrollListener = function () {
				var self = this;
				wizardBody.on("scroll", function () {
					wizardBody.find(".wizard-single-item").each(function () {
						var etop = $(this).offset().top;
						var diff = etop - $(window).scrollTop();
						if (diff > 50 && diff < 300) {
							wizardBody.find(".wizard-single-item").removeClass("next-wizard-item");
							wizardBody.find(".input-block input").each(function () {
								$(this).blur();
							});
							$(this).addClass("next-wizard-item");
						}
					});
				});
				wizardBody.on('swipe DOMMouseScroll mousewheel', function (e) {
					e.preventDefault();
					if (isScroll) return false;
					if (e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) {
						self.scrollNextStep('down', false);
					} else {
						self.scrollNextStep('up', false);
					}
				});
			};

			this.navigationListener = function () {
				var self = this;
				$('.wizard-up').click(function () {
					self.scrollNextStep('up', false);
				});
				$('.wizard-down').click(function () {
					self.scrollNextStep('down', false);
				});
			};

			this.updateProgressPercentage = function (obj) {
				var unfilled = 0;
				for (var step in obj) {
					if (obj[step] === null) {
						unfilled++;
					}
				}
				var updatedProgress = (((TOTAL_STEPS - unfilled) * 100) / TOTAL_STEPS);
				return Math.floor(updatedProgress);
			};

			this.updateUnfinishedTitle = function (obj) {
				var unfilled = [];
				var arr = [];
				for (var step in obj) {
					if (obj[step] === null) {
						unfilled.push(campaignSteps[step]);
					}
				}
				for (var i = 0; i < unfilled.length + 1; i++) {
					if (unfilled[i] !== undefined) {
						arr.push(unfilled[i]);
					}
				}
				return arr[0] || "Your campaign is ready!";
			};

			this.updateFinishedStep = function (obj) {
				var count = 0;
				for (var step in obj) {
					if (obj[step] === null) {
						count++;
					}
				}
				return (TOTAL_STEPS - count)
			};

			this.fillCampaignName = function (value, programName) {
				var campaignNameField = $('#wizardCampaignName');
				var year = value.substring(0, 4);
				campaignNameField.val(year + ' ' + programName + ' Challenge')
			};

			this.gotoUnfinished = function () {
				var self = this;
				var campaign = this.getCampaignFromStorage();
				if (campaign.updateProgressPercentage === 100) {
					wizardBody.scrollTo('#step1', '1000');
					currentStep = 1;
					self.updateProgressBar(15);
					setTimeout(function () {
						$('#step1').addClass('next-wizard-item');
						$('#step2').removeClass('next-wizard-item');
					}, '300');
					return 0;
				}
				var unfinished = campaign.updateFinishedStep + 1 || 0;
				currentStep = unfinished;
				if (unfinished < TOTAL_STEPS) {
					setTimeout(function () {
						wizardBody.scrollTo('#step' + unfinished, '1000');
						self.updateProgressBar(unfinished);
					}, 500);
				} else {
					setTimeout(function () {
						wizardBody.scrollTo('#step15', '1000');
						currentStep = 15;
					}, 500);
				}
			};

			this.handleCloseModal = function () {
				$('.close-confirm').click(function () {
					$('#confirmCampaign').modal('hide');
				});

				$('.continue-confirm').click(function () {
					$('#confirmCampaign').modal('hide');
					$('#createCampaign').modal('show');
				});

				$('#modal-close-confirm').click(function () {
					$('#confirmCampaign').modal('hide');
					$('#createCampaign').modal('show');
				});

				$('#createCampaign').on('hidden.bs.modal', function (e) {
					if (completed === true) {
						return $('#createCampaign').modal('hide');
					}
					if (currentStep <= 5) {
						$('#confirmCampaign').modal({
							backdrop: 'static',
							keyboard: false,
							show: true
						});
					} else {
						$('#createCampaign').modal('hide');
					}
				});
			};

			this.fillCampaignDates = function () {
				var campaign = this.getCampaignFromStorage();
				$('#disabledLaunchDate').val(campaign.launchDate);
				var keyDates = campaign.campaignKeyDates;
				if (keyDates) {
					$('#ciDate').val(keyDates.campaignInstructionDate);
					$('#ecbDate').val(keyDates.emailCampaignBeginsDate);
					$('#eventDate').val(keyDates.campaignEventDate);
					$('#et2Date').val(this.getKeyDate(keyDates.emailTouch2Date, 7));
					$('#et3Date').val(this.getKeyDate(keyDates.emailTouch3Date, 21));
					$('#et4Date').val(this.getKeyDate(keyDates.emailTouch4Date, 35));
					$('#et5Date').val(this.getKeyDate(keyDates.emailTouch5Date, 45));
				}
			};

			this.saveStep = function (field, step) {
				var self = this;
				$('.' + field + '-btn').click(function () {
					var campaign = self.getCampaignFromStorage();
					// Get the value from input source
					// Do additional things based on step
					var inputField = $('.' + field + '-input');
					var value = inputField.val();
					if (value === '') {
						inputField.focus().focusout();
						return false;
					}
					// Save the value to the localStorage
					if (field === 'launchDate') {
						self.fillCampaignName(value, campaign.programName);
						campaign.campaignKeyDates = self.setCampaignKeyDates(value);
					}
					// Fill participant goal
					if (field === 'expectedParticipantPercent') {
						self.calculateGoal();
					}
					if (field === 'campaignNeeds') {
						var checkboxInfo = [];
						var input = $('input[name=campaign-needs-checkbox]:checked');
						if (input) {
							input.each(function () {
								checkboxInfo.push($(this).val());
							});
							value = checkboxInfo;
						} else {
							value = [];
						}
					}
					if (field === 'additionalNeeds') {
						var additionalInputs = $('.additional-input-field');
						if (additionalInputs !== '') {
							var additionalNeeds = [];
							additionalInputs.each(function () {
								additionalNeeds.push($(this).val());
							});
							value = additionalNeeds;
						} else {
							value = ['No additional'];
						}
					}
					if (field === 'importantDate') {
						var val = $('#ecbDate').val();
						campaign.campaignKeyDates = self.setCampaignKeyDates(val);
					}

					if (step === 1) {
						$('#videoDescription').val(value + ' Promo');
					}

					if (step === 4) {
						$('#videoName').val(value + ' Video');
					}

					campaign[field] = value;

					// Update latest unfinished title
					campaign['updateUnfinishedTitle'] = self.updateUnfinishedTitle(campaign);
					// Update step (e.g 5 of 12);
					campaign['updateFinishedStep'] = self.updateFinishedStep(campaign);
					// Update bottom row percentage
					campaign['updateProgressPercentage'] = self.updateProgressPercentage(campaign);


					self.saveCampaignToStorage(campaign);

					// Update preview row
					self.updateCampaignRow();
					self.fillCampaignDates();

					// self.saveCampaignToStorage(campaign);
					// Go to next step
					currentStep = step;
					self.scrollNextStep('down', true);

					if (field === 'teamVideo') {
						completed = true;
						$('.preview-button-row').show();
						$('#createCampaign').modal('toggle');
					}

				})
			};
			this.updateStep = function (field, step) {
				var self = this;
				$('.' + field + '-btn').click(function () {
					currentStep = step;
					if (field === 'importantDates-yes') {
						var campaignEdit = $('#ecbDate');
						$('#ciDate').prop("disabled", false);
						campaignEdit.prop("disabled", false);
						campaignEdit.on('change', function () {
							var currentValue = $(this).val();
							$('#et2Date').val(self.getKeyDate(currentValue, 7));
							$('#et3Date').val(self.getKeyDate(currentValue, 21));
							$('#et4Date').val(self.getKeyDate(currentValue, 35));
							$('#et5Date').val(self.getKeyDate(currentValue, 45));
						});
					}
					self.scrollNextStep('down', true);
				})
			}

		};

		$(document).ready(function () {

			(function () {
				var wizard = new CampaignWizard();

				// Load init values to the localStorage
				wizard.loadInitToStorage();
				// Update campaign row on init
				wizard.updateCampaignRow();

				// Inputs configuration
				wizard.maskInputNumber('#wizardCampaignGoal');
				wizard.maskInputNumber('#participantGoal');
				wizard.maskLink('#vimeoVideo');
				wizard.setMinDate('#wizardDate');
				wizard.setMinDate('#ciDate');
				wizard.setMinDate('#ecbDate');

				// That will delete entire campaign on remove icon click
				wizard.clearCampaign('#clearDemoChallenge');

				// New campaign button listener
				wizard.newCampaignClick('.new-campaign');

				// Edit campaign
				wizard.editCampaignClick('.edit-campaign');

				// Handle close modal buttons
				wizard.handleCloseModal();

				// Scroll listeners
				// Go to the next step on mouse scroll
				wizard.scrollListener();
				// Go to the next step on button click
				wizard.navigationListener();

				// Save buttons listener
				// Campaign name step
				wizard.saveStep('programName', 1);
				// Already created campaigns list
				wizard.saveStep('continueCreating', 2);
				// Launch date
				wizard.saveStep('launchDate', 3);
				// Campaign name
				wizard.saveStep('campaignName', 4);
				// Campaign goal
				wizard.saveStep('campaignGoal', 5);
				// Expected Participation Percent
				wizard.saveStep('expectedParticipantPercent', 6);
				// Participant Goal
				wizard.saveStep('participantGoal', 7);
				// Campaign needs: 1 / 4
				wizard.updateStep('campaignNeeds-first', 8);
				// Campaign needs: 2 / 4
				wizard.updateStep('campaignNeeds-second', 9);
				// Campaign needs: 3 / 4
				wizard.updateStep('campaignNeeds-third', 10);
				// Campaign needs: 4 / 4
				wizard.saveStep('campaignNeeds', 11);
				// Additional needs
				wizard.saveStep('additionalNeeds', 12);
				// Edit or skip date customization
				wizard.updateStep('importantDates-yes', 13);
				wizard.updateStep('importantDates-no', 14);
				// Edit custom dates handler
				wizard.saveStep('importantDate', 14);
				// Save video upload and finish it
				wizard.saveStep('teamVideo', 15)

			})();

		});
	}
}
