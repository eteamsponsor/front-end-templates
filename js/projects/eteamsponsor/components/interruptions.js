var $ = require('jquery');
var bootbox = require('bootbox');

var interruptions = {
	formSubmit: {
		bindEvents: function() {
			var self = this;
			var $triggers = $('.interruption-form-submit-trigger');

			$triggers.data('confirm-submit', false);

			$triggers.validator({
				disable: false
			}).on('submit', function(e) {
				var $form = $(this);
				var formValid = !e.isDefaultPrevented();
				var submissionConfirmed = $form.data('confirm-submit');

				if (formValid && !submissionConfirmed) {
					e.preventDefault();

					self.interrupt($form.data('interruption-message'), $form.data('interruption-cancel-text'), $form.data('interruption-confirm-text'), $form);
				}
			});
		},
		interrupt: function(message, cancelButton, confirmButton, $form) {
			bootbox.confirm({
				message: message,
				buttons: {
					cancel: {
						label: cancelButton
					},
					confirm: {
						label: confirmButton
					}
				},
				callback: function(confirmed) {
					if (confirmed) {
						$form
							.data('confirm-submit', true)
							.submit();
					}
					else {
						$form.data('confirm-submit', false);
					}
				}
			});
		}
	},
	optionChange: {
		bindEvents: function() {
			var self = this;
			var $triggers = $('.interruption-option-change-trigger');

			// store the current values for potential later use
			$triggers.each(function() {
				$(this).data('current', $(this).val());
			});

			$triggers.on('change', function() {
				var $selectedOption = $(this).find(':selected');

				if ($selectedOption.hasClass('interruption-option-change')) {
					self.interrupt($selectedOption.data('interruption-message'), $selectedOption.data('interruption-cancel-text'), $selectedOption.data('interruption-confirm-text'), $(this));
				}
			});
		},
		interrupt: function(message, cancelButton, confirmButton, $select) {
			bootbox.confirm({
				message: message,
				buttons: {
					cancel: {
						label: cancelButton
					},
					confirm: {
						label: confirmButton
					}
				},
				callback: function(confirmed) {
					if (!confirmed) {
						$select
							.val($select.data('current'))
							.trigger('change');	// necessary for other change bindings to be heard, such as dynamic-form-ui
					}
				}
			});
		}
	},
	navigation: {
		bindEvents: function() {
			var self = this;
			var $triggers = $('.interruption-navigation-trigger');

			$triggers.on('click', function(e) {
				var $anchor = $(this);

				e.preventDefault();
				self.interrupt($anchor.data('interruption-message'), $anchor.data('interruption-cancel-text'), $anchor.data('interruption-confirm-text'), $anchor);
			});
		},
		interrupt: function(message, cancelButton, confirmButton, $anchor) {
			bootbox.confirm({
				message: message,
				buttons: {
					cancel: {
						label: cancelButton
					},
					confirm: {
						label: confirmButton
					}
				},
				callback: function(confirmed) {
					if (confirmed) {
						window.location.href = $anchor.attr('href');
					}
				}
			})
		}
	},
	navigationOptions: {
		bindEvents: function() {
			var self = this;
			var $triggers = $('.interruption-navigation-options');

			$triggers.on('click', function(e) {
				var $anchor = $(this);

				e.preventDefault();

				self.interrupt($anchor);
			});
		},
		interrupt: function($anchor) {
			var configObject = navigationOptions[$anchor.data('interruption-key')];
			var buttons = {};

			configObject.buttons.forEach(function(option) {
				buttons[option.name] = {
					label: option.label,
					className: option.class,
					callback: function() {
						window.location.href = option.url + ($anchor.data('interruption-append-to-url') || '');
					}
				};
			});

			bootbox.dialog({
				title: configObject.title,
				message: configObject.message,
				buttons: buttons
			});
		}
	}
};

for (var interruptionMethod in interruptions) {
	interruptions[interruptionMethod].bindEvents();
}