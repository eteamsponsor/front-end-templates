var $ = require('jquery');
var Quill = require('quill');

var landingAnchor = $('#campaign-emails-template');

if (landingAnchor.length > 0) {
	$(document).ready(function () {
		if (top.location.pathname === '/fundraker/campaign-emails.html') {
			function LandingEdit() {
				var editor;

				this.initLanding = {
					phone: '(800) 986-6128',
					phonePart: '(900) 123-4567',
					showAdmGoal: true,
					showPartGoal: true,
					aboutCampaign: {
						"ops": [
							{
								"insert": "Thank you for visiting our 2018 SHU Womens Soccer Challenge.\n" +
								"We appreciate any amount you can give and thank you in advance for your generous support."
							}
						]
					},
					aboutCampaignPart: {
						"ops": [
							{
								"insert": "Thank you for visiting my 2018 SHU Womens Soccer Challenge.\n" +
								"I appreciate any amount you can give and thank you in advance for your generous support."
							}
						]
					},
					defaultLanding: {
						"ops": [
							{
								"insert": "Thank you for visiting our 2018 SHU Womens Soccer Challenge.\n" +
								"We appreciate any amount you can give and thank you in advance for your generous support."
							}
						]
					}
				};
				this.loadInitToStorage = function () {
					var storage = localStorage.getItem('landing');
					if (!storage) {
						// Set to storage init values
						localStorage.setItem('landing', JSON.stringify(this.initLanding));
					} else if (storage === undefined) {
						// In case of localStorage bad init
						localStorage.clear();
					} else {
						console.log('Local Storage for a Landing: We good.')
					}
				};
				this.getCampaignFromStorage = function () {
					var campaignObject = localStorage.getItem('campaign');
					return JSON.parse(campaignObject);
				};
				this.getLandingFromStorage = function () {
					var emailsObject = localStorage.getItem('landing');
					return JSON.parse(emailsObject);
				};
				this.saveLandingToStorage = function (obj) {
					localStorage.setItem('landing', JSON.stringify(obj));
				};
				this.saveCampaignToStorage = function (obj) {
					localStorage.setItem('campaign', JSON.stringify(obj));
				};

				// Fill text to selector
				this.fillText = function (selector, target) {
					$(selector).html(target);
				};
				this.toggleEditView = function () {
					this.toggleBorderedClass('.landing-edit');
					this.toggleOpacityClass('.edit-opacity');
					this.toggleAllowEditClass('.no-click');
				};
				this.toggleView = function (selectors) {
					for (var i = 0; i < arguments.length; i++) {
						$(arguments[i]).toggle();
					}
				};
				this.toggleBorderedClass = function (selectors) {
					for (var i = 0; i < arguments.length; i++) {
						$(arguments[i]).toggleClass('bordered-editor');
					}
				};
				this.toggleOpacityClass = function (selectors) {
					for (var i = 0; i < arguments.length; i++) {
						$(arguments[i]).toggleClass('low-opacity');
					}
				};
				this.toggleAllowEditClass = function (selectors) {
					for (var i = 0; i < arguments.length; i++) {
						$(arguments[i]).toggleClass('disable-edit');
					}
				};
				this.initRTElanding = function (selector) {
					var rte = document.getElementById(selector);
					if (rte) {
						editor = new Quill(rte, {
							modules: {
								toolbar: true
							},
							theme: 'snow'
						});
					}
				};
				this.loadRTEContent = function (type) {
					if (editor) {
						var landing = this.getLandingFromStorage();
						var content;
						if (type === 'admin') {
							content = landing.aboutCampaign;
						} else if (type === 'part') {
							content = landing.aboutCampaignPart;
						}
						if (content.ops[0].insert.length === 1) {
							editor.setContents(landing.defaultLanding);
						} else {
							editor.setContents(content);
						}
						this.fillRTExt();
					}
				};
				this.fillRTExt = function () {
					if (editor) {
						var aboutField = $('#landing-about-campaign');
						this.fillText(aboutField, editor.root.innerHTML);
					}
				};
				this.saveRTE = function (selector, type) {
					var self = this;
					$(selector).click(function () {
						if (editor) {
							var landing = self.getLandingFromStorage();
							if (type === 'admin') {
								landing.aboutCampaign = editor.getContents();
							} else if (type === 'part') {
								landing.aboutCampaignPart = editor.getContents();
							}
							self.saveLandingToStorage(landing);
							if (landing.aboutCampaign.ops[0].insert.length === 1) {
								editor.setContents(landing.defaultLanding);
							}
							self.fillRTExt();
							$('#landingCloseRTE').click();
						}
					});
				};
				this.loadVideoPreview = function (selector) {
					var campaign = this.getCampaignFromStorage();
					var videoInput = $(selector);
					if (campaign !== null) {
						videoInput.val(campaign.teamVideo);
					}
				};
				this.saveLandingVideo = function (selector) {
					var video = $(selector);
					var campaign = this.getCampaignFromStorage();
					campaign.teamVideo = video.val();
					this.saveCampaignToStorage(campaign);
				};
				this.reInitState = function (type) {
					var campaign = this.getCampaignFromStorage();
					var landing = this.getLandingFromStorage();
					var landingCampaign = $('#landing-about-campaign');
					var partTitle = $('.preview-email-campaign-name-part');
					var showPercentage = $('.progress-goal-percentage');
					var goal, landingPhone;
					if (type === 'admin') {
						goal = $('.progress-amount-goal-value');
						landingPhone = $('#landing-phone');
						landingPhone.text(landing.phone);
						landingCampaign.text(landing.aboutCampaign.ops[0].insert);
						if (landing.showAdmGoal === true) {
							showPercentage.show();
						} else {
							showPercentage.hide();
						}
						partTitle.hide();
					} else if (type === 'part') {
						goal = $('.progress-amount-goal-part-value');
						landingPhone = $('#landing-part-phone');
						goal.html(campaign.participantGoal);
						landingPhone.html(landing.phonePart);
						landingCampaign.html(landing.aboutCampaignPart.ops[0].insert);
						partTitle.show();
						if (landing.showPartGoal === true) {
							showPercentage.show();
						} else {
							showPercentage.hide();
						}
					}
				};
				this.showAndHideGoal = function (obj) {
					var showAdminGoal = $('#editLandingGoalShow');
					var hideAdminGoal = $('#editLandingGoalHide');
					var showPartGoal = $('#editLandingPartGoalShow');
					var hidePartGoal = $('#editLandingPartGoalHide');
					var editLandingGoal = $('#editLandingGoal');
					var progressAdminSign = $('.progress-amount-goal');
					var progressPartSign = $('.progress-amount-part-goal');
					var adminGoalValue = $('#programAdminGoal');
					var partGoalValue = $('.progress-amount-goal-part-value');
					var goalPercentage = $('.progress-goal-percentage');
					showAdminGoal.hide();
					showPartGoal.hide();
					var landing = this.getLandingFromStorage();
					if (landing.showAdmGoal === true) {
						showAdminGoal.hide();
						hideAdminGoal.show();
						goalPercentage.show();
					}
					if (landing.showAdmGoal === false) {
						showAdminGoal.show();
						hideAdminGoal.hide();
						progressAdminSign.hide();
						adminGoalValue.hide();
						editLandingGoal.hide();
						goalPercentage.hide();
					}
					if (landing.showPartGoal === true) {
						showPartGoal.hide();
						hidePartGoal.show();
					}
					if (landing.showPartGoal === false) {
						showPartGoal.show();
						hidePartGoal.hide();
						progressPartSign.hide();
						partGoalValue.hide();
					}
					showAdminGoal.click(function () {
						landing.showAdmGoal = true;
						$(this).hide();
						hideAdminGoal.show();
						progressAdminSign.show();
						adminGoalValue.show();
						editLandingGoal.show();
						obj.saveLandingToStorage(landing);
						goalPercentage.show();
					});
					hideAdminGoal.click(function () {
						landing.showAdmGoal = false;
						$(this).hide();
						showAdminGoal.show();
						progressAdminSign.hide();
						editLandingGoal.hide();
						obj.saveLandingToStorage(landing);
						adminGoalValue.hide();
						$('#programAdminGoal').hide();
						goalPercentage.hide();
					});
					showPartGoal.click(function () {
						landing.showPartGoal = true;
						$(this).hide();
						hidePartGoal.show();
						progressPartSign.show();
						partGoalValue.show();
						obj.saveLandingToStorage(landing);
						goalPercentage.show();
					});
					hidePartGoal.click(function () {
						landing.showPartGoal = false;
						$(this).hide();
						showPartGoal.show();
						progressPartSign.hide();
						partGoalValue.hide();
						obj.saveLandingToStorage(landing);
						goalPercentage.hide();
					});
				};
				this.toggleLandingType = function (type, obj) {
					var self = this;
					var editAdmin = $('#editLandingAdmin');
					var editPart = $('#editLandingPart');
					var partTitle = $('.preview-email-campaign-name-part');
					var adminGoal = $('.landing-admin-goal');
					var partGoal = $('.landing-part-goal');
					var adminPhone = $('.landing-admin-phone');
					var partPhone = $('.landing-part-phone');
					var saveRTEadmin = $('#saveLanding');
					var saveRTEpart = $('#savePartLanding');
					var previewAdmin = $('.page-preview-admin');
					var previewPart = $('.page-preview-part');
					editPart.hide();
					partTitle.hide();
					partGoal.hide();
					partPhone.hide();
					saveRTEpart.hide();
					previewPart.hide();
					$('input[name="' + type + '"]').change(function () {
						var part = $('#' + type + '-part').is(':checked');
						var admin = $('#' + type + '-admin').is(':checked');
						if (self.editModeOn === true) {
							$('#canceLandingTemplateBtn').click();
						}
						if (part === true) {
							editPart.show();
							editAdmin.hide();
							partGoal.show();
							partPhone.show();
							saveRTEpart.show();
							adminGoal.hide();
							adminPhone.hide();
							saveRTEadmin.hide();
							self.reInitState('part');
							obj.loadRTEContent('part');
							previewAdmin.hide();
							previewPart.show();
						} else if (admin === true) {
							editPart.hide();
							editAdmin.show();
							partGoal.hide();
							partPhone.hide();
							adminGoal.show();
							adminPhone.show();
							saveRTEadmin.show();
							saveRTEpart.hide();
							previewAdmin.show();
							previewPart.hide();
							self.reInitState('admin');
							obj.loadRTEContent('admin');
						}
					});
				};
				this.saveAndCloseEdit = function () {
					var landing = this.getLandingFromStorage();
					if (landing !== null) {
						if (landing.showAdmGoal === false) {
							$('.progress-amount-goal-value').hide();
						}
					}
					this.editModeOn = false;
					if (this.editType === 'admin') {
						$('#editLandingAdmin').show();
						$('#landingRadio-admin').click();
					} else {
						$('#editLandingPart').show();
						$('#landingRadio-part').click();
					}

					$('.landing-preview-btns').hide();
					this.toggleEditView();
					$('.landing-edit-controls').hide();
					$('.vimeo-landing-builder').hide();
					$('#landing-about-editor').hide();
					$('#landing-about-body').show();
					$('.landing-video-area').show();
				}

			}

			$(document).ready(function () {

				function updateFields(obj) {
					var landingFields = obj.getLandingFromStorage();
					// Phone number
					var phoneField = $('#landing-phone');
					var phone = landingFields.phone;
					phoneField.attr('href', 'tel://' + phone);
					obj.fillText(phoneField, phone);
					var showPercentage = $('.progress-goal-percentage');
					if (landingFields.showAdmGoal === true) {
						showPercentage.show();
					}
				}

				function enableEditMode(selector, obj) {
					$('.landing-edit-controls').hide();
					$('#landing-about-editor').hide();
					$('.landing-preview-btns').hide();
					$('.vimeo-landing-builder').hide();
					$('#' + selector).click(function () {
						obj.editModeOn = true;
						$('.landing-edit-controls').toggle();
						$('.landing-preview-btns').show();
						$('.landing-video-area').hide();
						$('.vimeo-landing-builder').show();
						$(this).hide();
						obj.toggleEditView();
						if (selector === 'editLandingAdmin') {
							editLandingGoal(obj, 'admin');
							editLandingPhone(obj, 'admin');
							obj.editType = 'admin';
						} else if (selector === 'editLandingPart') {
							editLandingGoal(obj, 'part');
							editLandingPhone(obj, 'part');
							obj.editType = 'part';
						}
					});
				}

				function toggleRTE(selector, target, section, parent) {
					$(selector).click(function () {
						$(this).hide();
						$(target).show();
						$(section).toggle();
						$(parent).toggle();
					})
				}

				function editLandingGoal(obj, type) {
					var input, goal, editPrice, acceptPrice;
					var hideGoal = $('#editLandingGoalHide');
					if (type === 'admin') {
						input = $('#landingGoalInput');
						goal = $('.progress-amount-goal-value');
						editPrice = $('#editLandingGoal');
						acceptPrice = $('#editLandingGoalCheck');
					} else if (type === 'part') {
						input = $('#landingGoalPartInput');
						goal = $('.progress-amount-goal-part-value');
						editPrice = $('#editLandingPartGoal');
						acceptPrice = $('#editLandingPartGoalCheck');
					}

					acceptPrice.hide();
					input.hide();

					var campaign = obj.getCampaignFromStorage();
					var landing = obj.getLandingFromStorage();

					if (landing !== null) {
						if (landing.showAdmGoal === false) {
							hideGoal.hide();
							editPrice.hide();
						}
					}

					if (campaign !== null) {
						if (type === 'admin') {
							input.val(campaign.campaignGoal);
						} else if (type === 'part') {
							input.val(campaign.participantGoal);
						}
					}

					editPrice.click(function () {
						$(this).hide();
						goal.hide();
						acceptPrice.show();
						input.show();
						input.focus();
						hideGoal.hide();
						if (type === 'admin') {
							input.val(campaign.campaignGoal);
						} else if (type === 'part') {
							input.val(campaign.participantGoal);
						}
					});

					acceptPrice.click(function () {
						if (type === 'admin') {
							campaign.campaignGoal = input.val();
						} else if (type === 'part') {
							campaign.participantGoal = input.val();
						}
						obj.saveCampaignToStorage(campaign);
						goal.text(input.val());
						hideGoal.show();
						$(this).hide();
						goal.show();
						input.hide();
						editPrice.show();
					});

					// Goal input mask
					input.keypress(function (event) {
						if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 45 || event.which > 57)) {
							event.preventDefault();
						}
						if (!$(this).val() && event.which == 48) {
							event.preventDefault();
						}
					}).inputmask("numeric", {
						radixPoint: ".",
						groupSeparator: ",",
						digits: 0,
						min: 1,
						autoGroup: true,
						rightAlign: false
					});
				}

				function editLandingPhone(obj, type) {
					var input, phone, editBtn, acceptBtn;
					var donateText = $('.landing-donate-text');
					if (type === 'admin') {
						input = $('#landingPhoneInput');
						phone = $('#landing-phone');
						editBtn = $('#editLandingPhone');
						acceptBtn = $('#acceptLandingPhone');

					} else if (type === 'part') {
						input = $('#landingPartPhoneInput');
						phone = $('#landing-part-phone');
						editBtn = $('#editLandingPartPhone');
						acceptBtn = $('#acceptLandingPartPhone');
					}

					acceptBtn.hide();
					input.hide();
					var landing = obj.getLandingFromStorage();

					if (type === 'admin') {
						input.val(landing.phone);
					} else {
						input.val(landing.phonePart);
					}

					editBtn.click(function () {
						$(this).hide();
						phone.hide();
						donateText.hide();
						input.show();
						input.focus();
						acceptBtn.show();
						var num = phone.text();
						input.val(num);
					});

					acceptBtn.click(function () {
						$(this).hide();
						phone.show();
						donateText.show();
						input.hide();
						editBtn.show();
						if (type === 'admin') {
							landing.phone = input.val() || '(800) 123-1234';
						} else {
							landing.phonePart = input.val() || '(800) 123-1234';
						}
						obj.saveLandingToStorage(landing);
						if (input.val() === '') {
							phone.text('(800) 123-1234')
						} else {
							phone.text(input.val());
						}
					});

				}

				function saveAndCloseHandle(save, close, obj) {
					$(save).click(function () {
						obj.saveAndCloseEdit();
						obj.saveLandingVideo('#vimeoVideo');
					});
					$(close).click(function () {
						obj.saveAndCloseEdit();
					})
				}

				// Landing Page Edit

				(function () {
					var landing = new LandingEdit();

					// Load init values to storage
					landing.loadInitToStorage();
					updateFields(landing);

					// Init RTE editor
					landing.initRTElanding('landing-editor');
					landing.loadRTEContent('admin');

					// Toggle edit icon based on type
					landing.toggleLandingType('landingRadio', landing);

					// Deal with show and hide goal
					landing.showAndHideGoal(landing);

					// Enable landing edit mode
					enableEditMode('editLandingAdmin', landing);
					enableEditMode('editLandingPart', landing);

					// Show and Hide RTE listener
					toggleRTE('#landingEnableRTE', '#landingCloseRTE',
						'#landing-about-editor', '#landing-about-body');
					toggleRTE('#landingCloseRTE', '#landingEnableRTE',
						'#landing-about-editor', '#landing-about-body');

					// Load video link (if exist)
					landing.loadVideoPreview('#vimeoVideo');

					// Listen to save button
					landing.saveRTE('#saveLanding', 'admin');
					landing.saveRTE('#savePartLanding', 'part');

					saveAndCloseHandle('#saveLandingTemplateBtn', '#canceLandingTemplateBtn', landing);

				})()
			});
		}
	});
}
