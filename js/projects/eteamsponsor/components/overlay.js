var $ = require('jquery');
var browser = require('../../../components/browser');
var navigation = require('./navigation');

var overlay = {
	init: function() {
		var self = this;

		self.overlaySearch = $('.overlay-search');
		self.overlaySearchToggleTriggers = $('.overlay-search-toggle-trigger');
		self.overlayToggleTriggers = $('.overlay-toggle-trigger');

		self.bindEvents();
	},

	bindEvents: function() {
		var self = this;

		self.overlaySearchToggleTriggers.on('click', function(e) {
			e.preventDefault();

			self.overlaySearch.toggleClass('is-visible');

			browser.freezeViewport(300);
		});

		self.overlayToggleTriggers.on('click', function(e) {
			e.preventDefault();

			self.closeVisibleOverlay();
		});

		$(document).keyup(function(e) {
			// escape key
			if (e.keyCode == 27) {
				self.closeVisibleOverlay();
			}
		});
	},

	closeVisibleOverlay: function() {
		navigation.closeAllMenus();
		browser.thawViewport();

		$('.overlay.is-visible').removeClass('is-visible');
	}
};

overlay.init();
