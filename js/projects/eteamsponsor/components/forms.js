/* globals require */
var $ = require('jquery');
require('../../../components/jquery.mask');
require('../../../components/card-helper');
require('../../../components/floating-labels');
require('../../../components/file-input');
require('../../../components/jquery.placeholder');
require('../../../components/validator');
require('../../../components/dynamic-form-ui');
require('../../../components/emailValidation');

$(function() {
	'use strict';

	$('input, textarea').placeholder();

	$('.js-mask-phone').mask('(000) 000-0000');
	$('.js-mask-money').mask('$000,000,000,000,000');

	$('.class-of-alumnus').hide();
	$('#donorAlumnus').change(function(){
		if ($(this).val() === 'true') {
			$('.class-of-alumnus').show();
		} else {
			$('.class-of-alumnus').hide();
		}
	})
});
