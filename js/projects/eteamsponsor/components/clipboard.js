var Clipboard = require('../../../components/clipboard.min');
var bootbox = require('bootbox');
var $ = require('jquery');
var triggers = document.querySelectorAll('.js-clipboard');		// use a node list instead of a selctor in order to avoid collisions with the stopPropagation call in the nav; we want to be able to put clipboard classed elements in the nav

var clipboard = new Clipboard(triggers);

if (!Clipboard.isSupported()) {
	$('html').addClass('no-clipboard');
}

clipboard.on('success', function(e) {
	bootbox.alert($(e.trigger).data('clipboard-success-message'));
});