/* globals Modernizr, require */
var $ = require('jquery');
require('../../../components/card-helper');
require('../../../components/floating-labels');
require('../../../components/jquery.maskMoney');
require('../../../components/jquery.placeholder');
require('../../../components/validator');

$(function() {
	'use strict';
	var $sponsorshipAmountInput = $('[name="sponsorshipAmountInput"]'),
		initialSponsorshipAmount,
		initialValueSet = false,
		$sponsorshipAmountRadioAll = $('[name="sponsorshipAmountRadio"]'),
		$sponsorshipAmountRadioOther = $('#sponsorshipAmountRadioOther');

	if ($sponsorshipAmountInput.length > 0) {
		initialSponsorshipAmount = $sponsorshipAmountInput.val().replace('$', '');

		// initialize the checked radio state
		$sponsorshipAmountRadioAll.each(function() {
			var $radio = $(this);

			if (parseFloat($radio.val()).toFixed(2) === initialSponsorshipAmount) {
				$radio.get(0).checked = true;
				initialValueSet = true;

				// breaks out of the loop since the value was found
				return false;
			}
		});

		// if no initial value could be found, check the "Other" option
		if (!initialValueSet) {
			$sponsorshipAmountRadioOther.get(0).checked = true;
		}

		$sponsorshipAmountInput.maskMoney({
			precision: 2,
			prefix: '$'
		}).maskMoney('mask');	// second calling of maskMoney triggers a format of the value https://github.com/plentz/jquery-maskmoney/issues/171

		// update the sponsorship text input if a user changes radio selection
		$sponsorshipAmountRadioAll
			.not($sponsorshipAmountRadioOther)
			.on('change', function() {
				// after setting the value of the input, focusing and immediately blurring the input re-validates that one input
				$sponsorshipAmountInput
					.val(parseFloat($(this).val()).toFixed(2))
					.focus()
					.blur();
			});
		$sponsorshipAmountRadioOther
			.on('change', function() {
				// after setting the value of the input, focusing and immediately blurring the input re-validates that one input
				$sponsorshipAmountInput
					.val(parseFloat($(this).val()).toFixed(2))
					.focus()
			});

		// uncheck the current radio button selection if a user customizes their sponsorship amount
		$sponsorshipAmountInput.on('keyup', function() {
			var $checkedRadio = $('input[type="radio"]:checked');

			if ($checkedRadio.length > 0) {
				$checkedRadio[0].checked = false;
				$sponsorshipAmountRadioOther[0].checked = true;
			}
		});
	}

	$('input, textarea').placeholder();

	$('.prevent-multiple-submissions').on('submit', function(e) {
		var $submitButton = $(this).find('[type="submit"]');

		// See "Conditionally handling the submit event" on http://1000hz.github.io/bootstrap-validator/#validator-events
		if (!e.isDefaultPrevented()) {
			$submitButton
				.prop('disabled', true)
				.val('Please wait...');
		}
	});

	var date = new Date();
	var thisYear = date.getFullYear();
	var selectYear = $('#alumnus-year');
	var isAlumnusCheckbox = $('#is-alumnus');
	var classOf = $('#class-of');
	var confirmSubmit = $('#confirm-submit');
	var yearRequiredError = $('#year-required-error');
	var year = '-1';
	var isAlumnus = isAlumnusCheckbox[0].checked;

	function checkIsAlumnus(isAlumnus) {
		if (isAlumnus) {
			classOf.css('visibility', '');
			classOf.attr('required', true);
		} else {
			classOf.css('visibility', 'hidden');
			classOf.attr('required', false);
		}
	}

	yearRequiredError.hide();
	checkIsAlumnus(isAlumnus);

	for (var i = thisYear; i >= thisYear - 70; i--) {
		var val = i.toString();
		var option = $('<option></option>');
		option.append(val);
		option.attr('value', val);
		selectYear.append(option);
	}
	isAlumnusCheckbox.on('input', function(e) {
		isAlumnus = e.target.checked;
		checkIsAlumnus(isAlumnus);
	});
	selectYear.on('change', function(e) {
		year = e.target.value;
		if (e.target.value !== '-1') {
			yearRequiredError.hide();
		} else {
			yearRequiredError.show();
		}
	});
	confirmSubmit.on('click', function(e) {
		if (isAlumnus && year === '-1') {
			e.preventDefault();
			yearRequiredError.show();
		}
	});
});
