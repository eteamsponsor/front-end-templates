var $ = require('jquery');
require('../../../components/jquery.maskMoney');

$(function() {
	'use strict';
	// input values
	var $sponsorshipAmountInput = $('#sponsorshipAmountInput'),
		paymentFeePercent = parseFloat($('#payment-fee-percent').val()) || 3, // set default to 3% if there is no value
		paymentFlatFee = parseFloat($('#payment-flat-fee').val()) || 0; // set default to $0.00 if there is no value

	// output values
	var $processingFeeAmount = $('input[name=processingFeeAmount]'),
	    $payProcessingFee = $('input[name=payProcessingFee]'),
		$sponsorshipAmountToSend = $('input[name=sponsorshipAmount]');

	// elements
	var $processingFeeCheckbox = $('#processing-fee-checkbox'),
	    $sponsorshipAmountBtn = $('#sponsorship-amount-btn'),
	    $calculatedProcessingFee = $('#calculated-processing-fee'),
		$sponsorshipAmount = $('#sponsorship-amount');

	// values
	var sponsorshipAmountValueInit,
		calculatedFee,
		isChecked,
		amountValueText,
		previousValue,
		sponsorshipVerb;

	var parseValue = function(input) {
		return parseFloat(input.replace('$', '').replace(',', ''));
	};
	var calculateFee = function(sponsorshipAmountValue) {
		if (sponsorshipAmountValue && paymentFeePercent) {
			calculatedFee = parseFloat((parseFloat(sponsorshipAmountValue) * (paymentFeePercent / 100) + paymentFlatFee).toFixed(2));
			// display calculated processing fee
			$calculatedProcessingFee.text(calculatedFee.toFixed(2));
			// set data for the form param processingFeeAmount
			$processingFeeAmount.val(calculatedFee);
		}
	};
	var updateStaticFields = function(amountValue) {
		amountValueText = '$' + amountValue.toFixed(2);
		// display updated donation amount in button
		$sponsorshipAmountBtn.val(sponsorshipVerb + amountValueText);
		// display updated donation static amount
		$sponsorshipAmount.text(amountValueText);
	};
	var updateFields = function(inputValue, processingFee, checked) {
		var donationWithProcessingFeeValue = inputValue + (checked ? processingFee : 0);
		updateStaticFields(donationWithProcessingFeeValue);
		var amount = '$' + donationWithProcessingFeeValue.toFixed(2);
		// update input value
		$sponsorshipAmountInput.val(amount);
		// set data for the form param payProcessingFee
		$payProcessingFee.val(checked);
	};
	var applyCurrencyMask = function(element) {
		element.maskMoney({
			precision: 2,
			prefix: '$'
		}).maskMoney('mask');
	};
	var setSponsorshipAmount = function(value) {
		applyCurrencyMask($sponsorshipAmountToSend);
		var sponsorshipAmountToSend = isChecked ? (value - calculatedFee) : value;
		$sponsorshipAmountToSend.val('$' + sponsorshipAmountToSend.toFixed(2));
	};

	$processingFeeCheckbox.change(function() {
		isChecked = this.checked;
		var val = isChecked ? parseValue($sponsorshipAmountInput.val()) : previousValue;
		updateFields(val, calculatedFee, isChecked);
		previousValue = val;
		applyCurrencyMask($sponsorshipAmountInput);
	});

	$sponsorshipAmountInput.change(function(e) {
		calculateFee(parseValue(e.target.value));
		$processingFeeCheckbox.prop('checked', false);
		isChecked = false;
		applyCurrencyMask($sponsorshipAmountInput);
		setSponsorshipAmount(parseValue(e.target.value));
	});

	// on init
	if ($sponsorshipAmountInput.length > 0) {
		sponsorshipAmountValueInit = parseValue($sponsorshipAmountInput.val());
		$sponsorshipAmountInput.val(sponsorshipAmountValueInit.toFixed(2));
		setSponsorshipAmount(parseValue($sponsorshipAmountInput.val()));
		calculateFee(sponsorshipAmountValueInit);
		if ($sponsorshipAmountBtn.length > 0) {
			sponsorshipVerb = $sponsorshipAmountBtn.val().substring(0, $sponsorshipAmountBtn.val().indexOf('$'));
			updateStaticFields(sponsorshipAmountValueInit);
		}
		if ($payProcessingFee.length > 0) {
			$payProcessingFee.val(false);
		}
	}

}());
