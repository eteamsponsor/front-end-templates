/* globals global, require */
global.H5F = require('h5f');
require('../../components/ie10-viewport-bug-workaround');
require('../../components/svgxuse');
require('../../components/transition');
require('../../components/alert');
require('../../components/emailValidation');
require('../../components/sharing')({
    facebookAppId: '270230856648432'
});
require('./components/forms');
require('./components/processing-fee');
