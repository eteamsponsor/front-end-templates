var lib = require('./lib');
var multipleFieldsRegEx = /\s*,\s*/;

module.exports = function($input) {
	return {
		displayField: $input.data("typeahead-display-field") || "name",
		filterField: $input.data("typeahead-filter-field"),
		filterValue: $input.data("typeahead-filter-value"),
		highlightResults: true,
		minLength: $input.data("typeahead-minlength") || 3,
		notFoundTemplateSelector: typeof $input.data("typeahead-not-found-template") !== 'undefined' ? lib.getjQuerySafeSelector($input.data("typeahead-not-found-template")) : undefined,
		postBodyTemplate: {
			"query": {
				"bool": {
					"must": [
						{
							"multi_match": {}
						}
					]
				}
			},
			"sort": []
		},
		queryFields: ($input.data("typeahead-query-fields") || "name").split(multipleFieldsRegEx),
		sortFields: function() {
			// default to include the _score field first in all sorting
			var includeScore = typeof $input.data("typeahead-sort-include-score") === "undefined" ? true : $input.data("typeahead-sort-include-score");
			var scorePosition = typeof $input.data("typeahead-sort-score-position") === "undefined" ? "first" : $input.data("typeahead-sort-score-position");
			var sortFields = includeScore && scorePosition === "first" ? ["_score"] : [];
			var additionalSortFields = $input.data("typeahead-sort-fields");

			if (additionalSortFields) {
				sortFields = sortFields.concat(additionalSortFields.split(multipleFieldsRegEx));
			}

			sortFields = sortFields.map(function(sortField) {
				var sortFieldName = sortField.split(":")[0];
				var sortFieldDirection = sortField.split(":")[1];
				
				if (sortFieldName && sortFieldDirection) {
					sortField = {};
	
					sortField[sortFieldName] = sortFieldDirection;
				}

				return sortField;
			});
			
			if (includeScore && scorePosition === "last") {
				sortFields = sortFields.concat(["_score"]);
			}

			return sortFields;
		},
		suggestionCount: $input.data("typeahead-suggestion-count") || 5,
		suggestionSource: $input.data("typeahead-source"),
		suggestionTemplateSelector: typeof $input.data("typeahead-suggestion-template") !== 'undefined' ? lib.getjQuerySafeSelector($input.data("typeahead-suggestion-template")) : undefined
	};
};