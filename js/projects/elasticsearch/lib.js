var $ = jQuery;
var Handlebars = require('handlebars');

module.exports = {
	addFilterToPostBody: function(postBody, filterField, filterValue) {
		postBody.query.bool.filter = [
			{
				"term": {}
			}
		];

		postBody.query.bool.filter[0].term[filterField] = filterValue;

		return postBody;
	},

	getjQuerySafeSelector: function(selector) {
		if (this.isSelectorId(selector)) {
			selector = '[id="' + selector.substr(1) + '"]';
		}

		return selector;
	},

	getNotFoundTemplate: function(data, templateSelector) {
		var template = "<div class=\"tt-not-found\">No results were found that match your search.  Please try again.</div>";

		if (typeof templateSelector !== 'undefined') {
			template = Handlebars.compile($(templateSelector).html());

			return template(data);
		}

		return template;
	},

	getSuggestions: function(postBody, suggestionSource, query, queryFields, sortFields, asyncResults) {
		// assigns the post body's queryFields to the value entered in the input by the user
		postBody.query.bool.must[0].multi_match.query = query;
		postBody.query.bool.must[0].multi_match.fields = queryFields;
		postBody.sort = sortFields;

		$.post(suggestionSource, this.json.stringify(postBody), function(data) {
			asyncResults(data.hits.hits);
		}).fail(function(err) {
			alert("There was an error processing your request.  Please try again.");
		});
	},

	getSuggestionTemplate: function(data, templateSelector, displayField) {
		var template = "<div>" + data._source[displayField] + "</div>";

		if (typeof templateSelector !== 'undefined') {
			template = Handlebars.compile($(templateSelector).html());

			return template(data);
		}

		return template;
	},

	isSelectorId: function(selector) {
		return selector.indexOf('#') === 0;
	},

	// We are implementing a JSON stringify polyfill to avoid collisions with Prototype.js, which is loaded on many pages.
	// http://stackoverflow.com/questions/710586/json-stringify-array-bizarreness-with-prototype-js
	json: {
		parse: function(sJSON) { return eval('(' + sJSON + ')'); },
		stringify: (function () {
			var toString = Object.prototype.toString;
			var isArray = Array.isArray || function (a) { return toString.call(a) === '[object Array]'; };
			var escMap = {'"': '\\"', '\\': '\\\\', '\b': '\\b', '\f': '\\f', '\n': '\\n', '\r': '\\r', '\t': '\\t'};
			var escFunc = function (m) { return escMap[m] || '\\u' + (m.charCodeAt(0) + 0x10000).toString(16).substr(1); };
			var escRE = /[\\"\u0000-\u001F\u2028\u2029]/g;
			return function stringify(value) {
				if (value == null) {
					return 'null';
				} else if (typeof value === 'number') {
					return isFinite(value) ? value.toString() : 'null';
				} else if (typeof value === 'boolean') {
					return value.toString();
				} else if (typeof value === 'object') {
					if (typeof value.toJSON === 'function') {
						if (typeof(value) == "object") {
							return value.toJSON();
						}
						return stringify(value.toJSON());
					} else if (isArray(value)) {
						var res = '[';
						for (var i = 0; i < value.length; i++)
							res += (i ? ', ' : '') + stringify(value[i]);
						return res + ']';
					} else if (toString.call(value) === '[object Object]') {
						var tmp = [];
						for (var k in value) {
							if (value.hasOwnProperty(k))
								tmp.push(stringify(k) + ': ' + stringify(value[k]));
						}
						return '{' + tmp.join(', ') + '}';
					}
				}
				return '"' + value.toString().replace(escRE, escFunc) + '"';
			};
		})()
	}
};