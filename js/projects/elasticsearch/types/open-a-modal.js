var $ = jQuery;
var lib = require('../lib');

$('[data-typeahead-modal]').each(function() {
	var $input = $(this);
	var defaults = require('../defaults')($input);
	var modalContainer = lib.getjQuerySafeSelector($input.data("typeahead-modal"));
	var modalSource = $input.data("typeahead-modal-source");
	var appendToModalSourceKey = $input.data("typeahead-append-to-modal-source-key") || "id";
	var postBody = $.extend(true, {}, defaults.postBodyTemplate);

	if (typeof defaults.filterField !== 'undefined') {
		postBody = lib.addFilterToPostBody(postBody, defaults.filterField, defaults.filterValue);
	}

	$input.typeahead({
		highlight: defaults.highlightResults,
		minLength: defaults.minLength
	}, {
		display: function(data) {
			return data._source[defaults.displayField];
		},
		limit: defaults.suggestionCount,
		source: function(query, syncResults, asyncResults) {
			lib.getSuggestions(postBody, defaults.suggestionSource, query, defaults.queryFields, defaults.sortFields(), asyncResults);
		},
		templates: {
			suggestion: function(data) {
				return lib.getSuggestionTemplate(data, defaults.suggestionTemplateSelector, defaults.displayField);
			},
			notFound: function(data) {
				return lib.getNotFoundTemplate(data, defaults.notFoundTemplateSelector);
			}
		}
	}).on('typeahead:select typeahead:autocomplete', function(e, selection) {
		var modalRemote = appendToModalSourceKey === "none" ? modalSource : modalSource + selection._source[appendToModalSourceKey];

		$(modalContainer).modal({
			remote: modalRemote
		});

		// clear the Bootstrap modal data cache to enable users to select subsequent suggestions and get fresh content
		$(modalContainer).on('hide.bs.modal', function() {
			$(this).removeData('bs.modal');
		});
	});
});