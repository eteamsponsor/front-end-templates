var $ = jQuery;
var lib = require('../lib');

$('[data-typeahead-link]').each(function() {
	var $input = $(this);
	var defaults = require('../defaults')($input);
	var link = $input.data("typeahead-link");
	var appendToLinkKey = $input.data("typeahead-append-to-link-key") || "id";
	var postBody = $.extend(true, {}, defaults.postBodyTemplate);

	if (typeof defaults.filterField !== 'undefined') {
		postBody = lib.addFilterToPostBody(postBody, defaults.filterField, defaults.filterValue);
	}

	$input.typeahead({
		highlight: defaults.highlightResults,
		minLength: defaults.minLength
	}, {
		display: function(data) {
			return data._source[defaults.displayField];
		},
		limit: defaults.suggestionCount,
		source: function(query, syncResults, asyncResults) {
			lib.getSuggestions(postBody, defaults.suggestionSource, query, defaults.queryFields, defaults.sortFields(), asyncResults);
		},
		templates: {
			suggestion: function(data) {
				return lib.getSuggestionTemplate(data, defaults.suggestionTemplateSelector, defaults.displayField);
			},
			notFound: function(data) {
				return lib.getNotFoundTemplate(data, defaults.notFoundTemplateSelector);
			}
		}
	}).on('typeahead:select', function(e, selection) {
		window.location.href = link + selection._source[appendToLinkKey];
	});
});