require('../../components/svgxuse');
require('../../components/tab');
require('../../components/dropdown');
require('../../components/modal');
require('../../components/sharing')({
    facebookAppId: '108206192561870'
});
// require('./components/landing-preview');
require('./components/video');
require('./components/clipboard');
require('./components/progress-bar');
require('./components/dynamic-amount');
require('./components/donor-list');
