var $ = require('jquery');
var Quill = require('quill');

$(document).ready(function () {

	var challenge, challengeGoal, phone,
		landingBody, landing, campaign;

	if (top.location.pathname === '/fundraker/program-admin-preview-landing.html') {
		var pointerAdmin = $('#programLandingAdminPreview');
		if (pointerAdmin.length > 0) {
			challenge = $('.landing-challenge');
			challengeGoal = $('.landing-goal');
			phone = $('.landing-phone');
			landingBody = $('.landing-body');

			var camp = JSON.parse(localStorage.getItem('campaign'));


			landing = JSON.parse(localStorage.getItem('landing'));
			campaign = JSON.parse(localStorage.getItem('campaign'));

			var video = campaign.teamVideo;
			var campaignVideo = $('.preview-preview-campaign-video');
			if (camp.teamVideo) {
				var id = video.split('/').pop();
				campaignVideo.attr('src', '//player.vimeo.com/video/' + id)
			} else {
				campaignVideo.attr('src', '//player.vimeo.com/video/112532909')
			}
			if (campaign) {
				challenge.html(campaign.campaignName);
				challengeGoal.html('/ $ ' + campaign.campaignGoal);
				phone.html(landing.phone);
				phone.attr('href', 'tel://' + landing.phone);
			}

			var adminRteEditor;
			var adminRte = document.getElementById('landingPreviewAdminRTE');

			if (adminRte) {
				adminRteEditor = new Quill(adminRte, {
					modules: {
						toolbar: false
					},
					theme: 'snow'
				});
				var aboutCampaign = landing.aboutCampaign;
				adminRteEditor.setContents(aboutCampaign);
				landingBody.html(adminRteEditor.root.innerHTML);
			}
		}
	}
	if (top.location.pathname === '/fundraker/program-participant-preview-landing.html') {
		var pointerPart = $('#programLandingPartPreview');
		if (pointerPart.length > 0) {

			challenge = $('.landing-part-challenge');
			challengeGoal = $('.landing-part-goal');
			phone = $('.landing-part-phone');
			landingBody = $('.landing-preview-part-body');

			landing = JSON.parse(localStorage.getItem('landing'));
			campaign = JSON.parse(localStorage.getItem('campaign'));

			if (campaign) {
				var video = campaign.teamVideo;
				var campaignVideo = $('.preview-preview-campaign-video');
				if (campaign.teamVideo) {
					var id = video.split('/').pop();
					campaignVideo.attr('src', '//player.vimeo.com/video/' + id)
				} else {
					campaignVideo.attr('src', '//player.vimeo.com/video/112532909')
				}

				challenge.html(campaign.campaignName);
				challengeGoal.html('/ $ ' + campaign.participantGoal);
				phone.html(landing.phonePart);
				phone.attr('href', 'tel://' + landing.phonePart);
			}

			var partRteEditor;
			var partRte = document.getElementById('landingPreviewPartRTE');

			if (partRte) {
				partRteEditor = new Quill(partRte, {
					modules: {
						toolbar: false
					},
					theme: 'snow'
				});
				var aboutCampaignPart = landing.aboutCampaignPart;
				partRteEditor.setContents(aboutCampaignPart);
				landingBody.html(partRteEditor.root.innerHTML);
			}
		}
	}
});
