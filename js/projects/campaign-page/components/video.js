var $ = require('jquery');
var Vimeo = require('../../../components/vimeo-player.min.js');

$('.play').on('click', function(e) {
	e.preventDefault();

	var $posterDetails = $('.poster-details');
	var $posterShare = $('.poster-share');

	$posterDetails
		.add($posterShare)
		.addClass('is-hidden')
		.delay(300)
		.queue(function(next) {
			var campaignVideo = document.getElementById('campaign-video');
			var player = new Vimeo(campaignVideo);

			player.on('ended', function() {
				$posterShare.removeClass('is-hidden');
			});

			player
				.play()
				.then(function() {
				})
				.catch(function(err) {
					console.log(err);
				});

			next();
		});
});