var $ = require('jquery');

function dynamicAmount() {
	var progressAmountRaised = parseInt($('[data-amount-raised]')[0].dataset.amountRaised.replace(/,/g , ''));
	var init = 0;
	var growAmount = setInterval(grow, 1);
	function grow() {
		if (init >= progressAmountRaised) {
			clearInterval(growAmount);
		} else {
			if (progressAmountRaised - init > 1000) {
				init = init + 100;
			} else if (progressAmountRaised - init > 100) {
				init = init + 10;
			} else {
				init = init + 1;
			}
			$('.progress-amount-raised')[0].innerText = '$' + init.toLocaleString();
		}
	}
}

$(document).ready(function() {
	setTimeout(dynamicAmount, 1000);
});
