var $ = require('jquery');

$(document).ready(function() {
	var listContainer = $('.list-container');
	var loader = $('.loader');

	listContainer.hide();
	loader.show();

	var seeMoreBtn = $('.see-more');
	var donorsListUl = $('.donor-list');
	var total = $('[data-total]')[0].dataset.total || 0;
	var fundParticipantId = $('[data-fundparticipant]')[0].dataset.fundparticipant || '';
	var fundraiserId = $('[data-fundraiser]')[0].dataset.fundraiser || '';
	var fundParticipantIdQuery = 'fundParticipantId=' + fundParticipantId;
	var fundraiserIdQuery = '&fundraiserId=' + fundraiserId;
	var pager = 0;
	var offset = '&offset=' + pager;
	var url = 'getDonorsList?' + fundParticipantIdQuery + fundraiserIdQuery + offset;
	var windowLocation = window.location.origin;
	var fullUrl = windowLocation + "/ETS/publicAccess/" + url;

	function createLi (donor) {
		var donor = donor || null;
		var donorName;
		var donorLocation;
		var donorComment;
		var createdLi = "<li></li>";
		var innerDiv = "<div class='top'></div>";
		var tileDiv = "<div class='tile-light valign'></div>";
		if (donor.isDonationPublic) {
			donorName = "<div class='donor-name'>" + donor.indSponsorName + " - $" + donor.amount + "</div>";
			if (donor.billingAddressCity && donor.billingAddressState) {
				donorLocation = "<div class='donor-location'>" + donor.billingAddressCity + ", " + donor.billingAddressState.toUpperCase() + "</div>";
			}
		} else {
			donorName = "<div class='donor-name'>Anonymous - $" + donor.amount + "</div>";
		}
		if (donor.donorComment) {
			donorComment = "<div class='donor-comment'>" + donor.donorComment + "</div>";
		}

		return $(createdLi).append($(tileDiv).append($(innerDiv).append(donorName, donorLocation, donorComment)));
	}
	donorsListUl.empty();
	$.ajax({
		url: fullUrl,
		success: function(result){
			var donorsList = [];
			donorsList.push(result);
			donorsList[0].forEach(function (item) {
				donorsListUl.append(createLi(item));
			});
			loader.hide();
			listContainer.show();
		},
		error: function(error) {
			console.log(error);
		}
	});

	seeMoreBtn.on('click', function(e) {
		e.preventDefault();
		listContainer.hide();
		loader.show();
		pager += 5;
		if (pager > total) {
			pager = 0
		}
		offset = '&offset=' + pager;
		url = 'getDonorsList?' + fundParticipantIdQuery + fundraiserIdQuery + offset;
		fullUrl = windowLocation + "/ETS/publicAccess/" + url;
		$.ajax({
			url: fullUrl,
			success: function(result){
				var donorsList = [];
				donorsListUl.empty();
				donorsList.push(result);
				donorsList[0].forEach(function (item) {
					donorsListUl.append(createLi(item));
				});
				loader.hide();
				listContainer.show();
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
});
