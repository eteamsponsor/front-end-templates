/* globals Modernizr */
var breakpoints = require('./breakpoints');
var $ = require('jquery');
var scrolledAmount;

module.exports = {
	freezeViewport: function(delay) {
		var $body = $('body');

		delay = delay || 0;

		scrolledAmount = $body.scrollTop();

		setTimeout(function() {
			$body.css({
				'position': 'fixed',
				'top': -scrolledAmount
			});
		}, delay);
	},

	getSize: function() {
		var browserSize,
			supportsMediaQueries = module.exports.supportsMediaQueries();

		for (var breakpoint in breakpoints) {
			if (supportsMediaQueries) {
				if (Modernizr.mq('(min-width: ' + breakpoints[breakpoint] + 'px)')) {
					browserSize = breakpoint;
				}
				else {
					break;
				}
			}
			else {
				if (document.documentElement.clientWidth > breakpoints[breakpoint]) {
					browserSize = breakpoint;
				}
				else {
					break;
				}
			}
		}

		return browserSize;
	},

	supportsMediaQueries: function() {
		return Modernizr.mq('only all');
	},

	thawViewport: function() {
		$('body').css('position', 'static').scrollTop(scrolledAmount);
	}
};