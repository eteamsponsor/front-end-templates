var EmailValidation = {};
var $ = require('jquery');
require('./mailcheck');

EmailValidation.classNames = {
	suggestionContainer: 'mailcheck-suggestion',
	suggestionLink: 'mailcheck-suggestion-link'
};

EmailValidation.removeMailcheckSuggestion = function(element) {
	element.remove();
};

EmailValidation.defaults = {
	domains: [
		'gmail.com',
		'hotmail.com',
		'yahoo.com',
		'sbcglobal.net',
		'aol.com'
	],
	topLevelDomains: [
		'com',
		'net',
		'org',
		'edu',
		'gov',
		'info',
		'biz'
	]
};

EmailValidation.bindSuggestionLinkClick = function($parentElement, $emailInput, callback) {
	$parentElement.on('click', '.' + EmailValidation.classNames.suggestionLink, function(e) {
		var $suggestionLink = $(this);

		e.preventDefault();

		$emailInput.val($suggestionLink.text());
		EmailValidation.removeMailcheckSuggestion($parentElement.find('.' + EmailValidation.classNames.suggestionContainer));

		if (typeof callback === 'function') {
			callback();
		}
	});
};

$(function() {
	$('.js-validate-email').on('blur', function() {
		$(this).mailcheck({
			domains: EmailValidation.defaults.domains,
			topLevelDomains: EmailValidation.defaults.topLevelDomains,
			suggested: function($element, suggestion) {
				var $suggestionElement = $('<div role="alert" class="alert alert-warning alert-dismissible fade in ' + EmailValidation.classNames.suggestionContainer + '"><button data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button> Did you mean <a href="javascript:void(0)" class="' + EmailValidation.classNames.suggestionLink + '">' + suggestion.full + '</a>?</div>');
				var $parentFormGroup = $($element).closest('.form-group');
				var callback = function() {
					$suggestionElement.alert('close');
				};

				EmailValidation.removeMailcheckSuggestion($parentFormGroup.find('.' + EmailValidation.classNames.suggestionContainer));

				$parentFormGroup.append($suggestionElement);

				EmailValidation.bindSuggestionLinkClick($suggestionElement, $element, callback);
			},
			empty: function($element) {
				var $parentFormGroup = $($element).closest('.form-group');

				EmailValidation.removeMailcheckSuggestion($parentFormGroup.find('.' + EmailValidation.classNames.suggestionContainer));
			}
		});
	});
});
