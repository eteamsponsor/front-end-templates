/* globals FB, require, alert */
// https://developers.facebook.com/docs/javascript/howto/jquery/v2.6
module.exports = function(appId) {
	var $ = require('jquery');
	$(function() {
		$.ajaxSetup({ cache: true });
		$.getScript('//connect.facebook.net/en_US/sdk.js', function() {
			FB.init({
				appId: appId,
				version: 'v2.6'
			});
		});
		function shareOnFacebook($link) {
			var shareConfig = {
				method: 'share',
				href: $link.data('share-url') || window.location.href
			};
			FB.ui(shareConfig, function(response) {
				if (response && response.error_message) {
					alert('Post was not published.');
				}
			});
		}
		$('[data-share-platform="facebook"]').on('click', function(e) {
			e.preventDefault();
			var $link = $(this);
			if ($('[data-generate-on-click="facebook"]') && !$link.attr('data-share-url')) {
				setTimeout(function() {
					if ($link.attr('data-share-url')) {
						shareOnFacebook($link);
					}
				}, 500);
				return;
			}
			shareOnFacebook($link);
		});
	});
};
