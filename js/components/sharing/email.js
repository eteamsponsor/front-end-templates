var $ = require('jquery');

$(function() {
	$('[data-share-platform="email"]').on('click', function() {
		var $link = $(this);
		var shareBody = $link.attr('data-share-body');
		if (!shareBody) {
			return;
		}
		var shareFirstPart = $link.data('share-body-first-part') || '';
		var shareSecondPart = $link.data('share-body-second-part') || '';
		var body = shareFirstPart + shareBody + shareSecondPart;
		var recipient = $link.data('share-recipient') != null ? encodeURIComponent($link.data('share-recipient')) : "";
		var href = "mailto:" + recipient + "?enctype=text/html&subject=" + encodeURIComponent($link.data('share-subject')) + "&body=" + encodeURIComponent(body);
		document.location.href = href;
	});
});
