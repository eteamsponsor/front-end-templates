module.exports = function(config) {
	require('./generate-link-on-click');
    require('./facebook')(config.facebookAppId);
    require('./twitter');
    require('./email');
};
