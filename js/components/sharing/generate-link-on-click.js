module.exports = (function() {
	var $ = require('jquery');
	var Clipboard = require('../clipboard.min');
	var bootbox = require('bootbox');
	$(function() {
		$('[data-generate-on-click]').one('click', function() {
			var $link = $(this);
			var apiUrl = $link.data('generate-api-url');
			var id = $link.data('fund-participant-id');
			var type = $link.data('generate-on-click');
			var attr = $link.data('generate-attr');
			var data = $link.data('generate-data');
			var dataClipBoardSuccessMessage = $link.data('clipboard-success-message');
			var generatedLink = null;
			var isClipboard = attr === 'data-clipboard-text';
			if(!type || !id || generatedLink) {
				return;
			}
			var url = apiUrl + '/shortenSocialMediaLink?id=' + id + '&socialSite=' + type;
			if (type === 'twitter' || type === 'googleplus') {
				var windowPopUp = window.open('', '', 'toolbar=yes,scrollbars=yes,resizable=yes,top=200,left=400,width=550,height=520');
			}
			function GetIEVersion() {
				var sAgent = window.navigator.userAgent;
				var Idx = sAgent.indexOf('MSIE');
				// If IE, return version number.
				if (Idx > 0)
					return parseInt(sAgent.substring(Idx+ 5, sAgent.indexOf('.', Idx)));
				// If IE 11 then look for Updated user agent string.
				else if (!!navigator.userAgent.match(/Trident\/7\./)) {
					return 11;
				} else {
					return 0; //It is not IE
				}

			}
			$.ajax({
				url: url,
				success: function(result){
					generatedLink = data ? data + result : result;
					if (isClipboard && !$link.attr(attr)) {
						if (GetIEVersion() > 0) {
							$link.attr(attr, generatedLink);
							return;
						}
						var clipboard = new Clipboard('body', {
						text: function(trigger) {
							return generatedLink;
						}
						});
						$('body').on('click.ets-copy', function(e) {
							if (e.target.innerText === 'OK') {
								bootbox.hideAll();
								setTimeout(function() {
									clipboard.destroy();
									$('body').off('click.ets-copy');
								}, 1000);
							}
						});
						bootbox.dialog({
							closeButton: false,
							message: '<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -10px;">×</button>'
							+ dataClipBoardSuccessMessage +
							'<div style="margin-top: 15px; padding-top: 15px; text-align: right; border-top: 1px solid #e5e5e5">' +
							'<button class="btn btn-primary ets-copy-ok-btn" type="button" onfocus="true">OK</button></div>'
						});
					}
					$link.attr(attr, generatedLink);
					if (type === 'twitter' || type === 'googleplus') {
						windowPopUp.location.href = generatedLink;
					} else if (type === 'sms' || type === 'email') {
						$link[0].click();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		});
	});
})();
