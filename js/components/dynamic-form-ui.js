/* globals require */
var $ = require('jquery');
var handlebars = require('handlebars');

$(function() {
	var $triggers = $('.dynamic-form-ui-trigger');

	function showContainer(trigger) {
		var $trigger = $(trigger),
			selection = $trigger.val(),
			name = $trigger.data('dynamic-form-ui-name'),
			$container = $('.dynamic-form-ui-container[data-dynamic-form-ui-name="' + name + '"]'),
			$selectedOption = $trigger.find(':selected'),
			url = $selectedOption.data('dynamic-form-ui-url'),
			method = $selectedOption.data('dynamic-form-ui-method') || 'GET',
			show = $selectedOption.hasClass('dynamic-form-ui-show'),
			hide = $selectedOption.hasClass('dynamic-form-ui-hide');

		if (show) {
			$container
				.addClass('show')
				.removeClass('hidden');

			if (url && selection !== null) {
				$.ajax(url, {
					method: method,
					data: {
						selection: selection
					}
				})
				.done(function(data) {
					$.get('../data/templates/' + data.type + '.hbs', function(template) {
						var compiledTemplate = handlebars.compile(template);

						$container
							.html(compiledTemplate(data))
							.show();
					})
					.fail(function() {
						alert('There was an error retrieving the options template.  Please try again.');
					});
				})
				.fail(function() {
					alert('There was an error retrieving options from the database.  Please try again.');
				});
			}
		}
		else if (hide) {
			$container
				.addClass('hidden')
				.removeClass('show');
		}
	}

	$triggers.each(function() {
		showContainer(this);
	})
	.on('change', function() {
		showContainer(this);
	});
});
