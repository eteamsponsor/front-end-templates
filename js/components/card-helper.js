var $ = require('jquery');
var debounce = require('./debounce');
var Card = require('./card');

$(function() {
	var $creditCardForm = $('.credit-card-form'),
		creditCardHelperBuilt = false;

	var initializeCreditCardHelper = debounce(function() {
		var card;

		// only build the the credit card helper if:
		// 1. the browser supports it, through 3d transforms
		// 2. the device is large enough (768px or wider)
		// 3. the helper hasn't been built already
		if (!creditCardHelperBuilt) {
			card = new Card({
				form: '.credit-card-form',
				container: '.credit-card-wrapper',
				formSelectors: {
					numberInput: '#creditCardNumber',
					expiryInput: '#expirationDate',
					cvcInput: '#cvv'
				}
			});

			document.getElementById('creditCardNumber').addEventListener('payment.cardType', function(e) {
				document.getElementById('creditCardType').value = e.detail;
			});

			// simulate a keyup event to initialize cardholder's name
			var evt = document.createEvent('HTMLEvents');
			evt.initEvent('keyup', false, true);
			document.getElementById('name').dispatchEvent(evt);

			creditCardHelperBuilt = true;
		}
	}, 100);

	if ($creditCardForm.length > 0) {
		window.addEventListener('load', initializeCreditCardHelper);
	}
});