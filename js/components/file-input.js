/* globals console */
var $ = require('jquery');

$(function() {
	'use strict';

	var $fileInputs = $('.file-input-container');

	if ('FileReader' in window) {
		$fileInputs.each(function() {
			var $container = $(this),
				$input = $container.find('input[type="file"]'),
				$name = $container.find('.file-name');

			$container.addClass('file-reader-support');

			$input.on('change', function(e) {
				$name.text(e.target.files[0].name);
			});
		});
	}
	else {
		$fileInputs.each(function() {
			$(this).addClass('no-file-reader-support');
		});
	}
});
