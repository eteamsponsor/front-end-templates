// breakpoints that match the values in less/core/variables
module.exports = {
	"extraSmall": 320,
	"small": 768,
	"medium": 992,
	"large": 1200
};