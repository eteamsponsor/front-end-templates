/* globals require */
var $ = require('jquery');
var emptyInputValues = ['', null, '$0'];	// $0 is an "empty" value for currency masked inputs

$(function() {
	'use strict';

	var $hasFloatingLabel = $('.has-floating-label');

	function isNonEmpty($input) {
		return emptyInputValues.indexOf($input.val()) === -1;
	}

	function setFloatingLabels(element) {
		var $element = $(element),
			$floatingLabelGroup = $element.parent('.floating-label-group');

		if (isNonEmpty($element)) {
			$element.add($floatingLabelGroup).addClass('is-non-empty');
		}
		else {
			$element.add($floatingLabelGroup).removeClass('is-non-empty');
		}
	}

	$hasFloatingLabel.each(function() {
		setFloatingLabels(this);
	}).on('blur change', function() {
		setFloatingLabels(this);
	});
});